<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('asset_id');
            $table->string('from');
            $table->string('to');
            $table->boolean('trade')->default(0);
            $table->bigInteger('price');
            $table->string('asset')->default('native');
            $table->string('sequence');
            $table->text('xdr');
            $table->timestamps();
            $table->index(['asset_id', 'from', 'to', 'trade']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
