<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address_notifications', function (Blueprint $table) {
            $table->id();
            $table->string('address')->unique();
            $table->string('email');
            $table->json('data')->nullable();
            $table->timestamps();
            $table->index(['address', 'email']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('address_notifications');
    }
}
