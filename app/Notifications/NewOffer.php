<?php

namespace App\Notifications;

use App\Models\Asset;
use App\Models\Offer;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Cache;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewOffer extends Notification implements ShouldQueue
{
    use Queueable;

    public $asset;

    public $offer;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Asset $asset, Offer $offer)
    {
        $this->asset = $asset;
        $this->offer = $offer;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $asset = $this->asset;
        $offer = $this->offer;

        $isRecipientOnline = Cache::get('online-'.$offer->owner, false);

        if($isRecipientOnline) return true;

        $offer = Offer::find($offer->id);

        if(empty($offer))  return true;

        return (new MailMessage)
                    ->subject('New Offer on StellarNFT')
                    ->markdown('mail.new-offer', ['asset' => $asset, 'offer' => $offer]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
