<?php

namespace App\View\Components\Dashboard;

use Illuminate\View\Component;

class Menu extends Component
{

    public $menu;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $menu = [];

        $menu[] = [
            'type' => 'heading',
            'name' => 'Dashboard'
        ];
        $menu[] = [
            'type' => 'item',
            'name' => 'Home',
            'icon' => '<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"></path>',
            'route' => 'dashboard.index',
            'link' => route('dashboard.index')
        ];
        $menu[] = [
            'type' => 'item',
            'name' => 'Horizon',
            'icon' => '<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 12h14M5 12a2 2 0 01-2-2V6a2 2 0 012-2h14a2 2 0 012 2v4a2 2 0 01-2 2M5 12a2 2 0 00-2 2v4a2 2 0 002 2h14a2 2 0 002-2v-4a2 2 0 00-2-2m-2-4h.01M17 16h.01" />',
            'route' => 'horizon.index',
            'link' => route('horizon.index'),
            'target' => 'new'
        ];
        $menu[] = [
            'type' => 'item',
            'name' => 'Logs',
            'icon' => '<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />',
            'route' => 'dashboard.logs',
            'link' => route('dashboard.logs'),
            'target' => 'new'
        ];

        $menu[] = [
            'type' => 'heading',
            'name' => 'Marketplace'
        ];
        $menu[] = [
            'type' => 'item',
            'name' => 'Assets',
            'icon' => '<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 11H5m14 0a2 2 0 012 2v6a2 2 0 01-2 2H5a2 2 0 01-2-2v-6a2 2 0 012-2m14 0V9a2 2 0 00-2-2M5 11V9a2 2 0 012-2m0 0V5a2 2 0 012-2h6a2 2 0 012 2v2M7 7h10" />',
            'route' => 'dashboard.assets*',
            'link' => route('dashboard.assets.index')
        ];
        $menu[] = [
            'type' => 'item',
            'name' => 'Files',
            'icon' => '<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 7v10c0 2.21 3.582 4 8 4s8-1.79 8-4V7M4 7c0 2.21 3.582 4 8 4s8-1.79 8-4M4 7c0-2.21 3.582-4 8-4s8 1.79 8 4m0 5c0 2.21-3.582 4-8 4s-8-1.79-8-4" />',
            'route' => 'dashboard.files*',
            'link' => route('dashboard.files.index')
        ];
        $menu[] = [
            'type' => 'item',
            'name' => 'Offers',
            'icon' => '<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 10h16M4 14h16M4 18h16" />',
            'route' => 'dashboard.offers*',
            'link' => route('dashboard.offers.index')
        ];
        $menu[] = [
            'type' => 'item',
            'name' => 'Activities',
            'icon' => '<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 10h16M4 14h16M4 18h16" />',
            'route' => 'dashboard.activities*',
            'link' => route('dashboard.activities.index')
        ];
        $menu[] = [
            'type' => 'item',
            'name' => 'User Settings',
            'icon' => '<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 10h16M4 14h16M4 18h16" />',
            'route' => 'dashboard.settings*',
            'link' => route('dashboard.settings.index')
        ];
        $menu[] = [
            'type' => 'item',
            'name' => 'Reports',
            'icon' => '<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 10h16M4 14h16M4 18h16" />',
            'route' => 'dashboard.reports*',
            'link' => route('dashboard.reports.index')
        ];
        $menu[] = [
            'type' => 'item',
            'name' => 'Categories',
            'icon' => '<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 10h16M4 14h16M4 18h16" />',
            'route' => 'dashboard.categories*',
            'link' => route('dashboard.categories.index')
        ];
        $menu[] = [
            'type' => 'item',
            'name' => 'Forbidden Addresses',
            'icon' => '<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 10h16M4 14h16M4 18h16" />',
            'route' => 'dashboard.forbidden-addresses*',
            'link' => route('dashboard.forbidden-addresses.index')
        ];
        $menu[] = [
            'type' => 'item',
            'name' => 'Verified Addresses',
            'icon' => '<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 10h16M4 14h16M4 18h16" />',
            'route' => 'dashboard.verified-addresses*',
            'link' => route('dashboard.verified-addresses.index')
        ];

        $this->menu = $menu;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('dashboard.layouts.menu');
    }
}
