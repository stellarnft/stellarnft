<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\MarketplaceHelper;
use Illuminate\Support\Facades\Http;

class UpdateXLMPrice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'xlmprice:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update XLM price';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $response = Http::get('https://api.coingecko.com/api/v3/simple/price', [
                'ids' => 'stellar',
                'vs_currencies' => 'usd'
            ]);
        } catch (\Throwable $e) {
            die();
        }

        if($response->ok()) {
            $json = $response->json();
            if($json && !empty($json['stellar'])) {
                MarketplaceHelper::setXLMPrice($json['stellar']['usd']);
            }
        }
    }
}
