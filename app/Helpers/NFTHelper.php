<?php

namespace App\Helpers;

use App\Jobs\Asset\GetNewOwner;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;

class NFTHelper
{
    public static function getCacheKey($code = '', $issuer = '')
    {
        if(empty($code) || empty($issuer)) {
            return '';
        }

        return 'nft-'.strtoupper($code).'-'.strtoupper($issuer);
    }

    public static function checkOwner($asset, $cache = true) 
    {
        if(empty($asset->owner)) {
            GetNewOwner::dispatch($asset);
            return null;
        }

        $currentOwner = $asset->owner;

        if(self::isNFTOwner($asset->code, $asset->issuer, $currentOwner, $cache)) {
            return $currentOwner;
        } else {
            $asset->owner = null;
            $asset->save();
            AssetHelper::clearAssetCache($asset->code, $asset->issuer);
            GetNewOwner::dispatch($asset);
        }

        return null;
    }

    public static function getNFTAuthor($code, $issuer) 
    {
        $author = null;

        $horizonServer = config('site.horizonServer');

        $url = $horizonServer. 'accounts/'.$issuer . '/operations?limit=1';

        $response = Http::get($url);

        if($response->ok()) {
            $results = $response->json();
            if(!empty($results['_embedded']) && !empty($results['_embedded']['records'])) {
                $operations = $results['_embedded']['records'];
                $operation = $operations[0];
                if($operation['type'] == 'create_account') {
                    $author = $operation['funder'];
                }
            }
        }

        if(empty($author)) {
            $url = 'https://horizon.stellar.org/accounts/'.$issuer . '/operations?limit=1';

            $response = Http::get($url);

            if($response->ok()) {
                $results = $response->json();
                if(!empty($results['_embedded']) && !empty($results['_embedded']['records'])) {
                    $operations = $results['_embedded']['records'];
                    $operation = $operations[0];
                    if($operation['type'] == 'create_account') {
                        $author = $operation['funder'];
                    }
                }
            }
        }

        return $author;
    }  

    public static function getNFTOwner($code, $issuer) 
    {
        $horizonServer = config('site.horizonServer');

        $currentUrl = $horizonServer. 'accounts?asset=' . $code . ':' .$issuer . '&limit=200';

        $search = true;

        while($search) {
            $response = Http::get($currentUrl);

            if($response->ok()) {
                $results = $response->json();

                if(empty($results['_embedded']) || empty($results['_embedded']['records'])) return false;

                $links = $results['_links'];

                if($links['next']['href'] != $links['self']['href']) {
                    $currentUrl = $links['next']['href'];
                } else {
                    $search = false;
                }
                
                $accounts = $results['_embedded']['records'];

                foreach($accounts as $account) {
                    $balances = $account['balances'];

                    foreach($balances as $balance) {
                        if(empty($balance['asset_code']) || empty($balance['asset_issuer'])) continue;
                        
                        if($balance['asset_code'] == $code && $balance['asset_issuer'] == $issuer && $balance['balance'] == '0.0000001') {
                            return $account['id'];
                        };
                    }
                }
            } else {
                $search = false;
            } 
        }

        return false;
    }

    public static function isNFTOwner($code, $issuer, $publicKey, $cache = true) 
    {
        if($cache) {
            return Cache::remember(self::getCacheKey($code, $issuer).'-owner-'.$publicKey, 5, function () use ($publicKey, $code, $issuer) {
                $horizonServer = config('site.horizonServer');

                $response = Http::get($horizonServer. 'accounts/' . $publicKey);

                if($response->ok()) {
                    $account = $response->json();
                    
                    $balances = $account['balances'];

                    foreach($balances as $balance) {
                        if(empty($balance['asset_code']) || empty($balance['asset_issuer'])) continue;
                        
                        if($balance['asset_code'] == $code && $balance['asset_issuer'] == $issuer && $balance['balance'] == '0.0000001') return true;
                    }
                }

                return false;
            });    
        } else {
            $horizonServer = config('site.horizonServer');

            $response = Http::get($horizonServer. 'accounts/' . $publicKey);

            if($response->ok()) {
                $account = $response->json();
                
                $balances = $account['balances'];

                foreach($balances as $balance) {
                    if(empty($balance['asset_code']) || empty($balance['asset_issuer'])) continue;
                    
                    if($balance['asset_code'] == $code && $balance['asset_issuer'] == $issuer && $balance['balance'] == '0.0000001') return true;
                }
            }

            return false;
        }
    }

    public static function getNFT($code, $issuer) 
    {
        $nft = Cache::get(self::getCacheKey($code, $issuer), false);

        if(!empty($nft)) return $nft;

        $horizonServer = config('site.horizonServer');

        $locked = false;

        $nft = [];

        $response = Http::get($horizonServer. 'accounts/' . $issuer);

        if($response->ok()) {
            $account = $response->json();
            
            $signers = $account['signers'];

            if(count($signers) == 1 && $signers[0]['weight'] == 0) $locked = true;

            $nft['issuer'] = $account['id'];

            $data = (array) $account['data'];

            foreach($data as $key => $value) {
                $data[$key] = base64_decode($value);
            }

            $nft['dataentries'] = $data;

            $nft['home_domain'] = !empty($account['home_domain']) ? $account['home_domain'] : '';
        }

        if($locked) {
            $response = Http::get($horizonServer. 'assets', [
                'asset_issuer' => $issuer,
                'asset_code' => $code
            ]);
            if($response->ok()) {
                $assets = $response->json();
                $records = $assets['_embedded']['records'];
                if(count($records) > 1) return false;
                $asset = $records[0];
                if($asset['amount'] == '0.0000001') {
                    $nft['code'] = $asset['asset_code'];

                    Cache::put(self::getCacheKey($code, $issuer), $nft);

                    return $nft;
                }
            }
        }

        return false;
    }
}