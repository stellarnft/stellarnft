<?php
namespace App\Helpers;

use App\Models\File as FileModel;
use App\Jobs\MoveFileToObjectStorage;
use Illuminate\Support\Facades\Crypt;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class FileHelper
{
    public static function getFileId($path, $elementFolder, $type = "image") 
    {
        $path = Crypt::decryptString($path);

        $newPath = str_replace('tmp/', 'public/'.$elementFolder.'/', $path);
        Storage::move($path, $newPath);  

        $file = new FileModel;
        $file->path = $newPath;
        $file->type = $type;
        $file->save();

        if(config('site.useCloudStorage')) {
            MoveFileToObjectStorage::dispatch($file);    
        }

        return $file->id;
    }

    public static function getUpdatedFileId($file, $elementFolder, $type = "image") 
    {
        $file = Crypt::decryptString($file);

        $newPath = $newObjectPath = null;
        
        if($fileDB = FileModel::where('id', $file)->first()) {
            if($fileDB->path) {
                $fileParts = explode('/', $fileDB->path);
                $fileParts[1] = $elementFolder;
                $newPath = implode('/', $fileParts);
                Storage::copy($fileDB->path, $newPath);    
            } else {
                $fileParts = explode('/', $fileDB->object_path);
                $fileParts[0] = $elementFolder;
                $newObjectPath = implode('/', $fileParts);
                Storage::disk('s3')->copy($fileDB->object_path, $newObjectPath);
            }
        } else {
            $newPath = str_replace('tmp/', 'public/'.$elementFolder.'/', $file);
            Storage::move($file, $newPath);  
        }

        $file = new FileModel;
        $file->path = $newPath;
        $file->object_path = $newObjectPath;
        $file->type = $type;
        $file->save();
        
        if(config('site.useCloudStorage')) {
            MoveFileToObjectStorage::dispatch($file);    
        }

        return $file->id;
    }


    public static function moveFileToObjectStorage($fileId) 
    {
        $file = FileModel::find($fileId);

        $path = $file->path;

        if(empty($path)) {

            $fileParts = explode('/', $file->object_path);

            $elementFolder = $fileParts[0];
            $fileName = $fileParts[1];
            
            if(empty($file->thumbnail) && $file->type == 'image') {
                $file->thumbnail = self::createThumbnails(false, $elementFolder, $fileName, Storage::disk('s3')->get($file->object_path));
                $file->save();
            }

            return true; 
        } 

        $fileParts = explode('/', $path);

        $elementFolder = $fileParts[1];
        $fileName = $fileParts[2];

        $objectPath = str_replace('public/', '', $path);

        if(Storage::disk('s3')->put($objectPath, Storage::get($path), 'public')) {
            $file->path = null;
            $file->object_path = $objectPath;
            if($file->type == 'image') $file->thumbnail = self::createThumbnails(Storage::path($path), $elementFolder, $fileName);
            $file->save();  

            Storage::delete($path);
    
            return true;
        }

        return false;
    }

    public static function createThumbnails($path, $elementFolder, $fileName, $content = false) 
    {
        $thumbnail = [];

        $sizes = [
            260,
            992
        ];

        foreach($sizes as $size) {
            $image = self::createThumbnail($path, $size, $size, $content);
            $thumbPath = $elementFolder.'/s'.$size.'_'.$fileName;
            if(Storage::disk('s3')->put($thumbPath, $image, 'public')) {
                $thumbnail[$size] = $thumbPath;
            }
        }

        return $thumbnail;
    }

    public static function createThumbnail($path, $width, $height, $content = false)
    {
        if($content) {
            $path = $content;
        } 

        $img = Image::make($path)->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });    

        return $img->encode();
    }

    public static function getFileCryptPath($fileId) 
    {
        $file = FileModel::find($fileId);

        $path = '';

        if($file) {
            $path = Crypt::encryptString($file->path);
        }

        return $path;
    }

    public static function removeFile($fileId) 
    {
        if(empty($fileId)) return true;

        $file = FileModel::find($fileId);

        if(!empty($file)) {
            if($file->path) {
                Storage::delete($file->path);
            }
            if($file->object_path) {
                Storage::disk('s3')->delete($file->object_path);
            }
            if($file->thumbnail) {
                foreach($file->thumbnail as $thumb_path) {
                    Storage::disk('s3')->delete($thumb_path);    
                }
            }
            $file->delete();
        }

        return true;
    }
}