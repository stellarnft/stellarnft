<?php
namespace App\Helpers;

use App\Helpers\Stellar\Base32;
use App\Helpers\Stellar\CRC16XModem;

class StellarHelper
{
    private static $versionBytes = [
        'accountId' => 0x30,
        'seed'      => 0x90
    ];

    public static function isValidAccountId($accountId)
    {
        if (empty($accountId)) {
            return false;
        }

        try {
            $decoded = self::decodeCheck("accountId", $accountId);
        } catch (\Exception $e) {
            return false;
        }

        if(!is_array($decoded)) return false;

        return count($decoded) == 32;
    }

    public static function decodeCheck($versionByteName, $encoded)
    {
        if (!is_string($encoded)) {
            throw new \Exception("encoded argument must be of type String");
        }

        $base32 = new Base32();

        $decoded = $base32->decode($encoded, true);

        if (empty($decoded) || !is_array($decoded)) {
            return false;
        }

        $versionByte = $decoded[0];
        $payload = array_slice($decoded, 0, -2);

        $data = array_slice($payload, 1);
        $checksum = array_slice($decoded, -2);

        if ($base32->encode($base32->decode($encoded)) != $encoded) {
            return false;
        }

        $expectedVersion = self::$versionBytes[$versionByteName];
        if (empty($expectedVersion)) {
            return false;
        }

        if ($versionByte != $expectedVersion) {
            return false;
        }

        $expectedChecksum = self::calculateChecksum($payload);

        if (!self::verifyChecksum($expectedChecksum, $checksum)) {
            return false;
        }

        return $data;
    }

    private static function calculateChecksum($payload)
    {
        // This code calculates CRC16-XModem checksum of payload
        // and returns it as Buffer in little-endian order.
        $crc16 = new CRC16XModem();
        $crc16->update($payload);

        return self::uInt16($crc16->getChecksum(), 0);
    }

    private static function uInt16($value, $offset)
    {
        $value = +$value;
        $offset = $offset >> 0;

        $buffer = [];
        $buffer[$offset] = $value & 0xff;
        $buffer[$offset + 1] = $value >> 8;

        return $buffer;
    }

    private static function verifyChecksum($expected, $actual)
    {
        if (count($expected) !== count($actual)) {
            return false;
        }

        if (count($expected) === 0) {
            return true;
        }

        for ($i = 0; $i < count($expected); $i++) {
            if ($expected[$i] != $actual[$i]) {
                return false;
            }
        }

        return true;
    }
}