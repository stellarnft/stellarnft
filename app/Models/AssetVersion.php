<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssetVersion extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $with = ['image', 'video', 'audio', 'categories'];

    protected $casts = [
        'tradable' => 'boolean',
        'nsfw' => 'boolean',
        'private' => 'boolean'
    ];

    public function categories()
    {
        return $this->belongsToMany('App\Models\Category');
    }

    public function image()
    {
        return $this->hasOne('App\Models\File', 'id', 'image_id');
    }

    public function audio()
    {
        return $this->hasOne('App\Models\File', 'id', 'audio_id');
    }

    public function video()
    {
        return $this->hasOne('App\Models\File', 'id', 'video_id');
    }

    public function getImageUrlAttribute()
    {
        $url = asset('/images/no-image.svg');

        if($this->image) {
            $url = $this->image->url;
        }

        return $url;
    }

    public function thumb($size)
    {
        $url = asset('/images/no-image.svg');

        if($image = $this->image) {
            $url = $this->image->thumb($size);
        }

        return $url;
    }
}
