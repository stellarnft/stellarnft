<?php

namespace App\Models;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Asset extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $with = ['version'];

    protected $casts = [
        'banned' => 'boolean',
        'verified' => 'boolean',
        'dataentries' => 'array',
    ];

    public function getUrlAttribute()
    {
        return route('assets.show', ['code' => $this->code, 'issuer' => $this->issuer]);
    }

    public function getEditUrlAttribute()
    {
        return route('assets.edit', ['code' => $this->code, 'issuer' => $this->issuer]);
    }

    public function getUpdateUrlAttribute()
    {
        return route('assets.update', ['code' => $this->code, 'issuer' => $this->issuer]);
    }

    public function getDeleteUrlAttribute()
    {
        return route('assets.delete', ['code' => $this->code, 'issuer' => $this->issuer]);
    }

    public function getMakeOfferUrlAttribute()
    {
        return route('assets.make-offer', ['code' => $this->code, 'issuer' => $this->issuer]);
    }

    public function getOfferXdrUrlAttribute()
    {
        return route('assets.offer-xdr', ['code' => $this->code, 'issuer' => $this->issuer]);
    }

    public function getReportUrlAttribute()
    {
        return route('assets.report', ['code' => $this->code, 'issuer' => $this->issuer]);
    }

    public function version()
    {
        return $this->hasOne('App\Models\AssetVersion', 'id', 'asset_version_id');
    }

    public function offers()
    {
        return $this->hasMany('App\Models\Offer');
    }

    public function activities()
    {
        return $this->hasMany('App\Models\Activity');
    }

    protected static function booted()
    {
        static::created(function ($asset) {
            \App\Jobs\Asset\UpdateSearchable::dispatch($asset);
        });

        static::updated(function ($asset) {
            \App\Jobs\Asset\UpdateSearchable::dispatch($asset);
        });
    }
}
