<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Searchable extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $casts = [
        'active' => 'boolean',
        'banned' => 'boolean',
        'has_offers' => 'boolean',
        'nsfw' => 'boolean',
        'private' => 'boolean',
        'verified' => 'boolean',
        'sold_at' => 'datetime'
    ];

    public function assetCategories()
    {
        return $this->belongsToMany('App\Models\Category', 'asset_version_category', 'asset_version_id', 'category_id', 'asset_version_id');
    }

    public function scopePublicSearch($builder)
    {
        return $builder->where('active', 1)->where('banned', 0)->where('private', 0);
    }

    public function scopeSearch($builder, $searchQuery = '')
    {
        if(empty($searchQuery)) return $builder;

        return $builder->where(function($query) use ($searchQuery) {
            $query->orWhere('code', $searchQuery)
                ->orWhere('issuer', $searchQuery)
                ->orWhere('owner', $searchQuery)
                ->orWhere('name', 'like', '%'.$searchQuery.'%');
        });
    }

    public function scopeOrderResults($builder, $order = 'listing_date_asc')
    {
        switch ($order) {
            case 'listing_date_asc':
                $builder = $builder->orderBy('id');
                break;
            case 'last_sale_date':
                $builder = $builder->orderByDesc('sold_at')->orderByDesc('id');
                break;
            case 'price_asc':
                $builder = $builder->orderBy('price')->orderByDesc('id');
                break;
            case 'price_desc':
                $builder = $builder->orderByDesc('price')->orderByDesc('id');
                break;
            case 'highest_bounty':
                $builder = $builder->orderByDesc('bounty')->orderByDesc('id');
                break;
            default:
                $builder = $builder->orderByDesc('id');
        }

        return $builder;
    }

    public function scopeCategory($builder, $category = false)
    {
        $category = (int) $category;

        if(!empty($category) && $category > 0) {
            $builder->whereHas('assetCategories', function($query) use ($category) {
                $query->where('id', $category);
            });
        }

        return $builder;
    }

    public function scopeCategories($builder, $categories = [])
    {
        if(!empty($categories) && is_array($categories)) {
            $builder->whereHas('assetCategories', function($query) use ($categories) {
                $query->whereIn('id', $categories);
            });
        }

        return $builder;
    }

    public function scopeFilter($builder, $filter = [])
    {
        if(!in_array('nsfw', $filter)) {
            $builder = $builder->where('nsfw', 0);
        }

        if(in_array('has_bounty', $filter)) {
            $builder = $builder->where('bounty', '>', 0);
        }

        if(in_array('has_offers', $filter)) {
            $builder = $builder->where('has_offers', 1);
        }

        if(in_array('only_verified', $filter)) {
            $builder = $builder->where('verified', 1);
        }

        return $builder;
    }
}
