<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class VerifiedAddress extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected static function booted()
    {
        static::created(function ($verifiedAdresss) {
            \App\Jobs\Asset\UpdateVerification::dispatch($verifiedAdresss->address);
        });

        static::updated(function ($verifiedAdresss) {
            \App\Jobs\Asset\UpdateVerification::dispatch($verifiedAdresss->address);
        });

        static::deleted(function ($verifiedAdresss) {
            \App\Jobs\Asset\UpdateVerification::dispatch($verifiedAdresss->address);
        });
    }
}
