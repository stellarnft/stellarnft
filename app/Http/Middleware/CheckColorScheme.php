<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class CheckColorScheme
{
    const colorParam = 'colorScheme';
    const colorSchemes = ['dark', 'light'];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $cookieColorScheme = Cookie::get(self::colorParam);

        $requestColorScheme = $request->input(self::colorParam);

        $colorScheme = 'light';

        if(!empty($requestColorScheme) && in_array($requestColorScheme, self::colorSchemes)) {
            $colorScheme = $requestColorScheme;
        } elseif(!empty($cookieColorScheme) && in_array($cookieColorScheme, self::colorSchemes)) {
            $colorScheme = $cookieColorScheme;
        }

        Cookie::queue(self::colorParam, $colorScheme, 525600);

        session(['colorScheme' => $colorScheme]);

        return $next($request);
    }
}
