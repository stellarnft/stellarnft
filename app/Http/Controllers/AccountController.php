<?php

namespace App\Http\Controllers;

use App\Models\Offer;
use App\Helpers\StellarHelper;
use App\Models\Searchable;
use App\Helpers\AssetHelper;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['show', 'items', 'created', 'fetchItems', 'fetchCreated']);
    }

    public function show($address)
    {
        $this->checkAddress($address);

        return redirect()->route('account.items', $address);
    }

    public function items(Request $request, $address)
    {
        $this->checkAddress($address);

        return view('account.items', compact('address'));
    }

    public function created(Request $request, $address)
    {
        $this->checkAddress($address);

        return view('account.created', compact('address'));
    }

    public function fetchItems(Request $request, $address)
    {
        $this->checkAddress($address);

        $limit = (int) $request->limit;
        
        if($limit > 100) $limit = 100;

        $searchQuery = $request->search;

        $searchFilter = (array) $request->filter;

        $searchOrder = (string) $request->order;

        $category = $request->category;

        $assetsSearch = Searchable::where('owner', $address);

        if($address != session('publicKey'))  $assetsSearch = $assetsSearch->publicSearch();

        $assetsSearch = $assetsSearch->search($searchQuery)
                        ->category($category)
                        ->filter($searchFilter)
                        ->orderResults($searchOrder)
                        ->paginate($limit);

        
        $assets = $assetsSearch->items();

        $assets = AssetHelper::fromSearchablesToList($assets);

        return [
            'currentPage' => $assetsSearch->currentPage(),
            'assets' => $assets,
            'ended' => $assetsSearch->hasMorePages() ? false : true,
            'total' => $assetsSearch->total()
        ];
    }

    public function fetchCreated(Request $request, $address)
    {
        $this->checkAddress($address);

        $limit = (int) $request->limit;
        
        if($limit > 100) $limit = 100;

        $searchQuery = $request->search;

        $searchFilter = (array) $request->filter;

        $searchOrder = (string) $request->order;

        $assetsSearch = Searchable::where('author', $address);

        if($address != session('publicKey'))  $assetsSearch = $assetsSearch->publicSearch();

        $assetsSearch = $assetsSearch->search($searchQuery)
                        ->filter($searchFilter)
                        ->orderResults($searchOrder)
                        ->paginate($limit);

        
        $assets = $assetsSearch->items();

        $assets = AssetHelper::fromSearchablesToList($assets);

        return [
            'currentPage' => $assetsSearch->currentPage(),
            'assets' => $assets,
            'ended' => $assetsSearch->hasMorePages() ? false : true,
            'total' => $assetsSearch->total()
        ];
    }

    public function offersMade(Request $request, $address)
    {
        if(session('publicKey') != $address) abort(403);

        $offers = Offer::with('offer_asset')->where('from', session('publicKey'))->latest()->paginate(50);

        return view('account.offers.made', compact('address', 'offers'));
    }

    public function offersReceived(Request $request, $address)
    {
        if(session('publicKey') != $address) abort(403);

        $offers = Offer::with('offer_asset')->whereHas('offer_asset', function (Builder $query) {
            $query->where('owner', session('publicKey'));
        })->where('to', session('publicKey'))->latest()->paginate(50);

        return view('account.offers.received', compact('address', 'offers'));
    }

    public function claimableBalances(Request $request, $address)
    {
        if(session('publicKey') != $address) abort(403);

        return view('account.claimable-balances', compact('address'));
    }

    public function claimableBalancesRedirect(Request $request)
    {
        return redirect()->route('account.claimable-balances', session('publicKey'));
    }

    public function checkAddress($address)
    {
        if (!StellarHelper::isValidAccountId($address)) abort(404);

        return true;
    }
}
