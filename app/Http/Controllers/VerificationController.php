<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;
use App\Notifications\VerificationRequest;
use Illuminate\Support\Facades\Notification;

class VerificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('pages.verification');
    }

    public function send(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required'],
            'information' => ['required', 'min:30'],
            'screenshot' => ['required', 'url'],
        ], [
            'name.required' => 'The field is required.',
            'information.required' => 'The field is required.',
            'information.min' => 'The information must be at least :min characters.',
            'screenshot.required' => 'The field is required.',
            'screenshot.url' => 'Invalid url.',
        ]);

        $validator->validate();

        if(config('site.slack_hook'))
            Notification::route('slack', config('site.slack_hook'))
                ->notify(new VerificationRequest([
                    'name' => $request->name,
                    'information' => $request->information,
                    'screenshot' => $request->screenshot,
                    'publicKey' => session('publicKey')
                ]));

        return back()->withSuccess(__('Verification request successfully sent!'));
    }
}
