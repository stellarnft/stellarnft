<?php

namespace App\Http\Controllers;

use App\Helpers\FileHelper;
use App\Models\Asset;
use App\Helpers\PythonHelper;
use App\Helpers\StellarHelper;
use App\Helpers\NFTHelper;
use App\Models\Searchable;
use App\Helpers\AssetHelper;
use App\Helpers\OfferHelper;
use App\Models\AssetVersion;
use Illuminate\Http\Request;
use App\Jobs\Asset\GetAuthor;
use App\Helpers\ActivityHelper;
use App\Jobs\Asset\ClearOffers;
use App\Models\ForbiddenAddress;
use App\Jobs\Asset\DeleteVersion;
use App\Helpers\MarketplaceHelper;
use App\Jobs\Asset\DeleteVersions;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;
use App\Jobs\Activities\NewCreatedActivity;

class AssetController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'fetch', 'show']);
    }

    public function index()
    {
        return view('assets.index');
    }

    public function fetch(Request $request)
    {
        $limit = (int) $request->limit;
        
        if($limit > 100) $limit = 100;

        $searchQuery = $request->search;

        $searchFilter = (array) $request->filter;

        $category = $request->category;

        $searchOrder = (string) $request->order;

        $assetsSearch = Searchable::publicSearch()
                        ->search($searchQuery)
                        ->category($category)
                        ->filter($searchFilter)
                        ->orderResults($searchOrder)
                        ->paginate($limit);

        
        $assets = $assetsSearch->items();

        $assets = AssetHelper::fromSearchablesToList($assets);

        return [
            'currentPage' => $assetsSearch->currentPage(),
            'assets' => $assets,
            'ended' => $assetsSearch->hasMorePages() ? false : true,
            'total' => $assetsSearch->total()
        ];
    }

    public function show($code, $issuer)
    {
        $asset = Cache::rememberForever(AssetHelper::getCacheKey($code, $issuer), function() use ($code, $issuer) {
            $asset = Asset::where('code', $code)->where('issuer', $issuer)->firstOrFail();
            return $asset;
        });

        $asset->owner = NFTHelper::checkOwner($asset);

        if(!$asset->active) {
            AssetHelper::checkActive($asset);
        }

        $buyOffers = Cache::remember(OfferHelper::getCacheKey($asset->id, $asset->owner), 60, function() use ($asset) {
            return $asset->offers()->where('to', $asset->owner)->where('trade', 0)->orderByDesc('price')->get();
        });

        $activities = Cache::remember(ActivityHelper::getCacheKey($asset->id), 3600, function() use ($asset) {
            return $asset->activities()->orderByDesc('id')->get();
        });

        return view('assets.show', compact('asset', 'buyOffers', 'activities'));
    }

    public function add()
    {
        return view('assets.add');
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $validator_array = [
            'code' => ['required'],
            'issuer' => ['required', function ($attribute, $value, $fail) {
                if (!StellarHelper::isValidAccountId($value)) {
                    $fail('Enter correct Stellar Address');
                }
            }],
            'price' => [
                'required', 
                'numeric',
                'min:0.01',
                'max:50000000000',
                'regex:/^\d+(\.\d{1,2})?$/'
            ],
            'min_offer' => [
                'required', 
                'numeric',
                'min:0.01',
                'max:50000000000',
                'regex:/^\d+(\.\d{1,2})?$/'
            ],
            'bounty' => [
                'nullable',
                'numeric',
                'min:0.01',
                'max:'.(config('site.maxBountyReward') * 100),
                'regex:/^\d+(\.\d{1,2})?$/'
            ],
            'name' => ['required', 'max:42', 'string'],
            'description' => ['required', 'max:2000', 'string'],
            'categories' => [function ($attribute, $value, $fail) {
                if(empty($value)) return true;
                if(!is_array($value)) $fail(__('An error has occurred, try again later.'));
                $categories = MarketplaceHelper::getCategoriesArray();
                foreach($value as $item) {
                    if(is_array($item) || !in_array($item,  $categories)) $fail(__('An error has occurred, try again later.'));
                }
            }],
            'terms' => ['required']
        ];

        $validator = Validator::make($request->all(), $validator_array, [
            'code.required' => 'An error occurred, please try again later.',
            'issuer.required' => 'NFT Asset Issuer is required.',

            'min_offer.numeric' => 'The amount must be a number.',
            'min_offer.min' => 'The amount must be at least :min.',
            'min_offer.max' => 'The amount may not be greater than :max.',
            'min_offer.regex' => 'The amount has incorrect format.',

            'price.numeric' => 'The price must be a number.',
            'price.min' => 'The price must be at least :min.',
            'price.max' => 'The price may not be greater than :max.',
            'price.regex' => 'The price has incorrect format.',

            'name.required' => 'The name is required.',
            'name.max' => 'The name may not be greater than :max characters.',
            'description.required' => 'The description is required.',
            'description.max' => 'The description may not be greater than :max characters.',

            'bounty.numeric' => 'The bounty must be a number.',
            'bounty.min' => 'The bounty must be at least :min.',
            'bounty.max' => 'The bounty may not be greater than :max.',
            'bounty.regex' => 'The bounty must be between 0.01 and '.(config('site.maxBountyReward') * 100),

            'terms.required' => 'You must agree to the terms.',
        ]);

        $validator->validate();

        $code = $request->code;

        $issuer = $request->issuer;

        $asset = AssetHelper::getAssetByCode($code, $issuer);

        if($asset) {
            $request->session()->flash('warning', 'The asset already exists');

            return ['success' => true, 'url' => $asset->url];
        }

        $forbiddenAddress = ForbiddenAddress::where('address', $issuer)->first();

        if(!empty($forbiddenAddress)) {
            return response()->json([
                'errors' => [
                    'issuer' => [__('You cannot use this asset, please choose a different one.')]
                ]
            ], 422);
        }

        $nft = NFTHelper::getNFT($code, $issuer);
        if($nft == false) {
            return response()->json([
                'errors' => [
                    'issuer' => [__('Selected asset is not an NFT')]
                ]
            ], 422);
        }

        $isOwner = NFTHelper::isNFTOwner($code, $issuer, session('publicKey'));
        if($isOwner == false) {
            return ['success' => false];
        }

        $keypair = PythonHelper::getKeypair();

        if($keypair == false) return ['success' => false];

        $assetVersion = new AssetVersion;
        $assetVersion->code = $nft['code'];
        $assetVersion->issuer = $nft['issuer'];
        $assetVersion->price = round($request->price * config('site.priceRatio'));
        $assetVersion->min_offer = round($request->min_offer * config('site.priceRatio'));
        $assetVersion->bounty = !empty($request->bounty) ? round($request->bounty / config('site.stroop')) : 0;
        $assetVersion->name = $request->name;
        $assetVersion->description = $request->description;
        $assetVersion->lockable = trim($request->lockable);
        $assetVersion->tradable = false;
        if(!empty($request->nsfw)) $assetVersion->nsfw = true;
        $assetVersion->private = false;
        if(!empty($request->private)) $assetVersion->private = true;
        $assetVersion->save();
        if(!empty($request->image)) {
            $assetVersion->image_id = FileHelper::getFileId($request->image, 'asset-version-'.$assetVersion->id);
            $assetVersion->save();
        }
        if(!empty($request->video)) {
            $assetVersion->video_id = FileHelper::getFileId($request->video, 'asset-version-'.$assetVersion->id, 'video');
            $assetVersion->save();
        }
        if(!empty($request->audio)) {
            $assetVersion->audio_id = FileHelper::getFileId($request->audio, 'asset-version-'.$assetVersion->id, 'audio');
            $assetVersion->save();
        }
        $assetVersion->categories()->sync((array) $request->categories);

        $asset = new Asset;
        $asset->asset_version_id = $assetVersion->id;
        $asset->owner = session('publicKey');
        $asset->code = $nft['code'];
        $asset->issuer = $nft['issuer'];
        $asset->escrow_public_key = $keypair['public'];
        $asset->escrow_secret_key = $keypair['secret'];
        $asset->dataentries = $nft['dataentries'];
        $asset->home_domain = $nft['home_domain'];
        $asset->save();

        GetAuthor::dispatch($asset);

        NewCreatedActivity::dispatch($asset, [
            'from' => $asset->owner
        ]);

        return ['success' => true, 'url' => $asset->url];
    }

    public function edit($code, $issuer)
    {
        $asset = Cache::rememberForever(AssetHelper::getCacheKey($code, $issuer), function() use ($code, $issuer) {
            $asset = Asset::where('code', $code)->where('issuer', $issuer)->firstOrFail();
            return $asset;
        });

        if(!$asset->active) {
            return redirect($asset->url)->withDanger('The item needs to be activated.');
        }

        if($asset->owner != session('publicKey')) {
            return redirect($asset->url)->withDanger('Only the owner can edit the item.');
        }

        $categories = [];

        $assetCategories = $asset->version->categories ? $asset->version->categories->pluck('id') : [];

        $categoriesArray = MarketplaceHelper::getCategoriesArray();

        foreach($assetCategories as $assetCategory) {
            if(in_array($assetCategory, $categoriesArray)) $categories[] = $assetCategory;
        }

        $assetData = [
            'data' => [
                'name' => $asset->version->name,
                'description' => $asset->version->description,
                'lockable' => $asset->version->lockable,
                'image' => !empty($asset->version->image_id) ? Crypt::encryptString($asset->version->image_id) : '',
                'audio' => !empty($asset->version->audio_id) ? Crypt::encryptString($asset->version->audio_id) : '',
                'video' => !empty($asset->version->video_id) ? Crypt::encryptString($asset->version->video_id) : '',
                'price' => $asset->version->price / config('site.priceRatio'),
                'min_offer' => $asset->version->min_offer / config('site.priceRatio'),
                'bounty' => $asset->version->bounty ? ($asset->version->bounty * config('site.stroop')) : '',
                'categories' => $categories,
                'nsfw' => $asset->version->nsfw ? [1] : [],
                'private' => $asset->version->private ? [1] : []
            ],
            'nft' => [
                'issuer' => $asset->issuer,
                'code' => $asset->code
            ]
        ];

        return view('assets.edit', compact('asset', 'assetData'));
    }

    public function update(Request $request, $code, $issuer)
    {
        $asset = Cache::rememberForever(AssetHelper::getCacheKey($code, $issuer), function() use ($code, $issuer) {
            $asset = Asset::where('code', $code)->where('issuer', $issuer)->firstOrFail();
            return $asset;
        });

        if(!$asset->active) {
            return ['success' => false];
        }

        if($asset->owner != session('publicKey')) {
            return ['success' => false];
        }

        $data = $request->all();

        $validator_array = [
            'price' => [
                'required', 
                'numeric',
                'min:0.01',
                'max:50000000000',
                'regex:/^\d+(\.\d{1,2})?$/'
            ],
            'min_offer' => [
                'required', 
                'numeric',
                'min:0.01',
                'max:50000000000',
                'regex:/^\d+(\.\d{1,2})?$/'
            ],
            'bounty' => [
                'nullable',
                'numeric',
                'min:0.01',
                'max:'.(config('site.maxBountyReward') * 100),
                'regex:/^\d+(\.\d{1,2})?$/'
            ],
            'name' => ['required', 'max:42', 'string'],
            'description' => ['required', 'max:2000', 'string'],
            'categories' => [function ($attribute, $value, $fail) {
                if(empty($value)) return true;
                if(!is_array($value)) $fail(__('An error has occurred, try again later.'));
                $categories = MarketplaceHelper::getCategoriesArray();
                foreach($value as $item) {
                    if(is_array($item) || !in_array($item,  $categories)) $fail(__('An error has occurred, try again later.'));
                }
            }],
            'terms' => ['required']
        ];

        $validator = Validator::make($request->all(), $validator_array, [
            'min_offer.numeric' => 'The amount must be a number.',
            'min_offer.min' => 'The amount must be at least :min.',
            'min_offer.max' => 'The amount may not be greater than :max.',
            'min_offer.regex' => 'The amount has incorrect format.',

            'price.numeric' => 'The price must be a number.',
            'price.min' => 'The price must be at least :min.',
            'price.max' => 'The price may not be greater than :max.',
            'price.regex' => 'The price has incorrect format.',

            'name.required' => 'The name is required.',
            'name.max' => 'The name may not be greater than :max characters.',
            'description.required' => 'The description is required.',
            'description.max' => 'The description may not be greater than :max characters.',

            'bounty.numeric' => 'The bounty must be a number.',
            'bounty.min' => 'The bounty must be at least :min.',
            'bounty.max' => 'The bounty may not be greater than :max.',
            'bounty.regex' => 'The bounty must be between 0.01 and '.(config('site.maxBountyReward') * 100),

            'terms.required' => 'You must agree to the terms.',
        ]);

        $prevAssetVersion = $asset->version;

        $validator->validate();

        $assetVersion = new AssetVersion;
        $assetVersion->code = $asset->code;
        $assetVersion->issuer = $asset->issuer;
        $assetVersion->price = round($request->price * config('site.priceRatio'));
        $assetVersion->min_offer = round($request->min_offer * config('site.priceRatio'));
        $assetVersion->bounty = !empty($request->bounty) ? round($request->bounty / config('site.stroop')) : 0;
        $assetVersion->name = $request->name;
        $assetVersion->description = $request->description;
        $assetVersion->lockable = trim($request->lockable);
        $assetVersion->tradable = false;
        $assetVersion->private = false;
        if(!empty($request->private)) $assetVersion->private = true;
        if(!empty($request->nsfw)) $assetVersion->nsfw = true;
        $assetVersion->save();
        if(!empty($request->image)) {
            $assetVersion->image_id = FileHelper::getUpdatedFileId($request->image, 'asset-version-'.$assetVersion->id);
            $assetVersion->save();
        }
        if(!empty($request->video)) {
            $assetVersion->video_id = FileHelper::getUpdatedFileId($request->video, 'asset-version-'.$assetVersion->id, 'video');
            $assetVersion->save();
        }
        if(!empty($request->audio)) {
            $assetVersion->audio_id = FileHelper::getUpdatedFileId($request->audio, 'asset-version-'.$assetVersion->id, 'audio');
            $assetVersion->save();
        }
        $assetVersion->categories()->sync((array) $request->categories);

        $asset->asset_version_id = $assetVersion->id;
        $asset->save();

        AssetHelper::clearAssetCache($code, $issuer);

        DeleteVersion::dispatch($prevAssetVersion);

        return ['success' => true, 'url' => $asset->url];
    }

    public function delete(Request $request, $code, $issuer)
    {
        $asset = Cache::rememberForever(AssetHelper::getCacheKey($code, $issuer), function() use ($code, $issuer) {
            $asset = Asset::where('code', $code)->where('issuer', $issuer)->firstOrFail();
            return $asset;
        });

        if(!$asset->active) {
            return ['success' => false];
        }

        $asset->owner = NFTHelper::checkOwner($asset, false);

        if($asset->owner != session('publicKey')) {
            return ['success' => false];
        }

        $xdr = $request->xdr;

        $tx = AssetHelper::deleteAsset($xdr, $asset);

        if(!$tx || empty($tx['successful']) || !$tx['successful']) return ['success' => false];

        $assetId = $asset->id;

        $asset->delete();

        AssetHelper::clearAssetCache($code, $issuer);

        ClearOffers::dispatch($assetId);

        DeleteVersions::dispatch($code, $issuer);

        $request->session()->flash('success', 'The item has been deleted!');

        return ['success' => true, 'url' => route('account.items', session('publicKey'))];
    }
}
