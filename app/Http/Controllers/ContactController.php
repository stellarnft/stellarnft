<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Notifications\ContactRequest;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;

class ContactController extends Controller
{
    public function index()
    {
        $number = rand(1000000, 9999999);

        $salt = Str::random(40);

        session(['contactSalt' => $salt]);

        $encryptedNumber = Crypt::encryptString($number.$salt);

        return view('pages.contact', compact('number', 'encryptedNumber'));
    }

    public function send(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required'],
            'email' => ['required', 'email'],
            'message' => ['required', 'min:10'],
            'botTest' => [
                function ($attribute, $value, $fail) use ($request) {
                    $decryptedString = Crypt::decryptString($request->encryptedNumber);
                    $salt = substr($decryptedString, 7);
                    $contactSalt = session('contactSalt', '');
                    session(['contactSalt' => '']);
                    $number = $contactSalt == $salt ? substr($decryptedString, 0, 7) : rand();
                    if ($value != $number) {
                        $fail('An error occurred, please try again later.');
                    }
                }
            ],
        ])->setAttributeNames([
            'name' => __('name'),
            'email' => __('email'),
            'message' => __('message')
        ]);

        $validator->validate();

        if(config('site.slack_hook'))
            Notification::route('slack', config('site.slack_hook'))
                ->notify(new ContactRequest($request->all()));

        return back()->withSuccess(__('Message successfully sent!'));
    }
}
