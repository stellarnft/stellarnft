<?php

namespace App\Jobs\Asset;

use App\Helpers\AssetHelper;
use App\Models\AssetVersion;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class DeleteVersion implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $assetVersion;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(AssetVersion $assetVersion)
    {
        $this->assetVersion = $assetVersion;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $assetVersion = $this->assetVersion;

        AssetHelper::deleteAssetVersion($assetVersion);
    }
}
