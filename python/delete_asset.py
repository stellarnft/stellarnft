from datetime import datetime
from stellar_sdk import Keypair, Network, helpers, Asset, Claimant, TransactionBuilder, Account, Server, AccountMerge
from stellar_sdk.exceptions import BadSignatureError

class DeleteAsset:
    def __init__(self, owner, xdr, horizon, site_secret, escrow_secret, test, app):
        self.owner = owner
        self.xdr = xdr
        self.horizon = horizon
        self.site_secret = site_secret
        self.escrow_secret = escrow_secret
        self.test = test
        self.app = app

    def delete(self):

        passphrase = Network.PUBLIC_NETWORK_PASSPHRASE

        if self.test:
            passphrase = Network.TESTNET_NETWORK_PASSPHRASE

        transactionEnvelop = helpers.parse_transaction_envelope_from_xdr(self.xdr, passphrase)

        transaction = transactionEnvelop.transaction

        tx_hash = transactionEnvelop.hash()

        escrow_keypair = Keypair.from_secret(self.escrow_secret)

        keypair = transaction.source

        if self.owner != keypair.public_key:
            self.app.logger.warning('Source Not Match Owner Error')
            return False

        if len(transaction.operations) != 1:
            self.app.logger.warning('Operations Error')
            return False

        merge_op = transaction.operations[0]
        if not isinstance(merge_op, AccountMerge):
            self.app.logger.warning('Not AccountMerge Error')
            return False
        if merge_op.source != escrow_keypair.public_key:
            self.app.logger.warning('AccountMerge Source Not Match Escrow Error')
            return False
        if merge_op.destination != keypair.public_key:
            self.app.logger.warning('AccountMerge Destination Not Match Source Error')
            return False

        transactionEnvelop.sign(escrow_keypair)

        signers = [self.owner, escrow_keypair.public_key]

        for i in range(len(signers)):
            signer_public_key = signers[i]
            signer_keypair = Keypair.from_public_key(signer_public_key)
            signer_found = False
            signature_used = set()
            for index, decorated_signature in enumerate(transactionEnvelop.signatures):
                if index in signature_used:
                    continue
                try:
                    signer_keypair.verify(tx_hash, decorated_signature.signature.signature)
                    signer_found = True
                    signature_used.add(index)
                    break
                except BadSignatureError:
                    pass

            if signer_found != True:
                self.app.logger.warning('No signer ' + signer_public_key)
                return False

        site_keypair = Keypair.from_secret(self.site_secret)

        fee_bump_tx = TransactionBuilder.build_fee_bump_transaction(
            fee_source=site_keypair,
            base_fee=200,
            inner_transaction_envelope=transactionEnvelop, network_passphrase=passphrase
        )
        fee_bump_tx.sign(site_keypair)

        server = Server(horizon_url=self.horizon)

        response = server.submit_transaction(fee_bump_tx)

        return response