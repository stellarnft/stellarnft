<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AssetController;
use App\Http\Controllers\OfferController;
use App\Http\Controllers\ReportController;

Route::get('/assets', [AssetController::class, 'index'])->name('assets.index');
Route::post('/assets/fetch', [AssetController::class, 'fetch'])->name('assets.fetch');

Route::get('/assets/{code}-{issuer}', [AssetController::class, 'show'])->name('assets.show');
Route::get('/assets/{code}-{issuer}/edit', [AssetController::class, 'edit'])->name('assets.edit');
Route::put('/assets/{code}-{issuer}/update', [AssetController::class, 'update'])->name('assets.update');
Route::post('/assets/{code}-{issuer}/delete', [AssetController::class, 'delete'])->name('assets.delete');

Route::get('/assets/add', [AssetController::class, 'add'])->name('assets.add');
Route::post('/assets/store', [AssetController::class, 'store'])->name('assets.store');

Route::post('/assets/{code}-{issuer}/offer', [OfferController::class, 'makeOffer'])->name('assets.make-offer');
Route::post('/assets/{code}-{issuer}/offer-xdr', [OfferController::class, 'offerXDR'])->name('assets.offer-xdr');
Route::post('/offer/{id}/cancel', [OfferController::class, 'cancel'])->name('offers.cancel');
Route::post('/offer/{id}/accept', [OfferController::class, 'accept'])->name('offers.accept');

Route::post('/assets/{code}-{issuer}/report', [ReportController::class, 'report'])->name('assets.report');

require __DIR__.'/account.php';