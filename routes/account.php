<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\SettingController;

Route::get('/u/{address}', [AccountController::class, 'show'])->name('account.show');
Route::get('/u/{address}/items', [AccountController::class, 'items'])->name('account.items');
Route::get('/u/{address}/created', [AccountController::class, 'created'])->name('account.created');
Route::post('/u/{address}/items/fetch', [AccountController::class, 'fetchItems'])->name('account.fetch-items');
Route::post('/u/{address}/created/fetch', [AccountController::class, 'fetchCreated'])->name('account.fetch-created');
Route::get('/u/{address}/offers-made', [AccountController::class, 'offersMade'])->name('account.offers-made');
Route::get('/u/{address}/offers-received', [AccountController::class, 'offersReceived'])->name('account.offers-received');
Route::get('/u/{address}/claimable-balances', [AccountController::class, 'claimableBalances'])->name('account.claimable-balances');

Route::get('/myaccount/claimable-balances', [AccountController::class, 'claimableBalancesRedirect'])->name('account.claimable-balances-redirect');



Route::get('/settings', [SettingController::class, 'index'])->name('settings.index');

Route::get('/settings/profile', [SettingController::class, 'profileSettings'])->name('settings.profile');
Route::put('/settings/profile/set', [SettingController::class, 'setProfileSettings'])->name('settings.profile.set');

Route::get('/settings/general', [SettingController::class, 'generalSettings'])->name('settings.general');
Route::post('/settings/general/set', [SettingController::class, 'setGeneralSettings'])->name('settings.general.set');

Route::get('/settings/notifications', [SettingController::class, 'notifications'])->name('settings.notifications');
Route::post('/settings/notifications/set', [SettingController::class, 'setNotifications'])->name('settings.notifications.set');