<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\NftController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\VerificationController;

require __DIR__.'/dashboard.php';

Route::get('/', [HomeController::class, 'index'])->name('home');

require __DIR__.'/assets.php';

Route::get('/nft/create', [NftController::class, 'create'])->name('nfts.create');
Route::post('/nft/validate', [NftController::class, 'validateData'])->name('nfts.validate');
Route::get('/nft/{code}-{issuer}', [NftController::class, 'show'])->name('nfts.show');

Route::get('/files/{name}/restore', [FileController::class, 'restore'])->name('file.restore');
Route::post('/files/{name}/process', [FileController::class, 'upload'])->name('file.upload');
Route::delete('/files/{name}/process', [FileController::class, 'delete'])->name('file.delete');

Route::view('/faq', 'pages.faq')->name('pages.faq');
Route::view('/help', 'pages.help')->name('pages.help');
Route::view('/bounty-program', 'pages.bounty-program')->name('pages.bounty-program');
Route::get('/contact', [ContactController::class, 'index'])->name('pages.contact');
Route::post('/contact/submit', [ContactController::class, 'send'])->name('contact.submit');
Route::get('/verification', [VerificationController::class, 'index'])->name('pages.verification');
Route::post('/verification/submit', [VerificationController::class, 'send'])->name('verification.submit');

Route::get('/storage/asset-version-{id}/{file}', [FileController::class, 'show']);

require __DIR__.'/auth.php';