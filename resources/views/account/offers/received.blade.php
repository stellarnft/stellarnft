@extends('layouts.app')

@section('title', 'Offers Received | '.config('app.name'))


@section('content')
    <x-profile.header :address="$address"></x-profile.header>

    <div class="container mx-auto sm:px-6 lg:px-8 py-6">
    @if($offers->isNotEmpty())
        <div class="relative w-full mx-auto sm:max-w-3xl mt-6 bg-white dark:bg-gray-700 shadow overflow-hidden sm:rounded-lg">
            <div class="overflow-auto">
                <table class="min-w-full divide-y divide-gray-300">
                    <thead>
                        <tr>
                            <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-400 uppercase tracking-wider">
                                Item
                            </th>
                            <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-400 uppercase tracking-wider">
                                Price
                            </th>
                            <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-400 uppercase tracking-wider">
                                XDR
                            </th>
                            <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-400 uppercase tracking-wider">
                                Created
                            </th>
                            <th scope="col" class="relative px-3 py-3"></th>
                        </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-300">
                        @foreach($offers as $offer)
                        <tr class="bg-gray-50">
                            <td class="px-3 py-4 whitespace-nowrap">
                                <a href="{{ $offer->offer_asset->url }}" class="text-purple-500">{{ $offer->offer_asset->version->name }}</a>
                            </td>
                            <td class="px-3 py-4 whitespace-nowrap">
                                <div class="flex items-center">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="flex-none h-4 w-4 mr-1" fill="currentColor" viewBox="0 0 236.36 200" xml:space="preserve">
                                        <path d="M203,26.16l-28.46,14.5-137.43,70a82.49,82.49,0,0,1-.7-10.69A81.87,81.87,0,0,1,158.2,28.6l16.29-8.3,2.43-1.24A100,100,0,0,0,18.18,100q0,3.82.29,7.61a18.19,18.19,0,0,1-9.88,17.58L0,129.57V150l25.29-12.89,0,0,8.19-4.18,8.07-4.11v0L186.43,55l16.28-8.29,33.65-17.15V9.14Z"/>
                                        <path d="M236.36,50,49.78,145,33.5,153.31,0,170.38v20.41l33.27-16.95,28.46-14.5L199.3,89.24A83.45,83.45,0,0,1,200,100,81.87,81.87,0,0,1,78.09,171.36l-1,.53-17.66,9A100,100,0,0,0,218.18,100c0-2.57-.1-5.14-.29-7.68a18.2,18.2,0,0,1,9.87-17.58l8.6-4.38Z"/>
                                    </svg>
                                    <span>{{ formatPrice($offer->price) }}</span>
                                </div>
                            </td>
                            <td class="px-3 py-4 whitespace-nowrap">
                                <a href="{{ $offer->lab_url }}" target="new-offer-xdr" class="text-purple-500">View</a>
                            </td>
                            <td class="px-3 py-4 whitespace-nowrap">
                                {{ $offer->created_at->diffForHumans() }}
                            </td>
                            <td class="px-3 py-4 whitespace-nowrap text-right text-sm font-medium">
                                @if($offer->to == session('publicKey'))
                                <offer-accept from-address="{{ $offer->from }}" price="{{ $offer->price / config('site.priceRatio') }}" xdr="{{ $offer->xdr }}" wallet="{{ session('wallet') }}" user-public-key="{{ session('publicKey') }}" action="{{ route('offers.accept', $offer->id) }}" :test="{{ App::environment('production') ? 'false' : 'true' }}" #default="{ show }">
                                    <x-button @click="show">Accept</x-button>    
                                </offer-accept>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="w-full sm:max-w-3xl">
            {{ $offers->links() }}
        </div>
        @else
        <div class="mt-6 sm:text-center">
            <div class="text-2xl font-thin">No offers found</div>
        </div>
        @endif
    </div>
@endsection