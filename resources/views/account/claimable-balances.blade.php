@extends('layouts.app')

@section('title', 'Claimable Balances | '.config('app.name'))


@section('content')
    <x-profile.header :address="$address"></x-profile.header>

    <div class="container mx-auto sm:px-6 lg:px-8 py-6">
        <assets-claimable-balances wallet="{{ session('wallet') }}" user-public-key="{{ session('publicKey') }}" action="{{ route('assets.store') }}" :test="{{ App::environment('production') ? 'false' : 'true' }}"></assets-claimable-balances>
    </div>
@endsection