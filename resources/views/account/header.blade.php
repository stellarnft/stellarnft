<div class="top h-64 w-full bg-purple-200 overflow-hidden relative" >
    @if($profileData['profile_banner'])
    <img src="{{ $profileData['profile_banner']->thumb(1920) }}" alt="" class="bg w-full h-full object-cover object-center absolute z-0">
    @endif
</div>
<div class="relative -mt-24">
    <div class="mx-auto relative w-full sm:max-w-md bg-white shadow sm:rounded-3xl">
        <div class="z-10 absolute -top-16 left-1/2 transform -translate-x-1/2">
            @if($profileData['profile_image'])
            <img src="{{ $profileData['profile_image']->thumb(260) }}" class="h-32 w-32 object-cover rounded-full">
            @else
            <div class="h-32 w-32 rounded-full bg-gradient-to-r from-purple-500 to-purple-300"></div>
            @endif
            @if(\App\Helpers\MarketplaceHelper::checkVerified($address))
            <div class="absolute bottom-0 right-0 text-green-600 tooltip tooltip-top" data-tooltip="Verified">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-8 w-8" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M6.267 3.455a3.066 3.066 0 001.745-.723 3.066 3.066 0 013.976 0 3.066 3.066 0 001.745.723 3.066 3.066 0 012.812 2.812c.051.643.304 1.254.723 1.745a3.066 3.066 0 010 3.976 3.066 3.066 0 00-.723 1.745 3.066 3.066 0 01-2.812 2.812 3.066 3.066 0 00-1.745.723 3.066 3.066 0 01-3.976 0 3.066 3.066 0 00-1.745-.723 3.066 3.066 0 01-2.812-2.812 3.066 3.066 0 00-.723-1.745 3.066 3.066 0 010-3.976 3.066 3.066 0 00.723-1.745 3.066 3.066 0 012.812-2.812zm7.44 5.252a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd" />
                </svg>
            </div>
            @endif
        </div>
        <div class="relative text-center pt-16 p-4">
            <h1 class="text-2xl font-semibold">{{ $profileData['name'] ? $profileData['name'] : 'Unnamed' }}</h1>
            <a class="text-sm text-purple-500" target="new-profile-address" href="https://stellar.expert/explorer/public/account/{{ $address }}">{{ truncStellarAddress($address) }}</a>
            @if($profileData['bio'])
            <h4 class="text-sm font-light mt-2">{!! nl2br(strip_tags($profileData['bio'])) !!}</h4>
            @endif

            <div class="mt-4 flex items-center justify-center space-x-3">
                @if($profileData['twitter'])
                <a href="{{ $profileData['twitter'] }}" target="_blank" class="transition-transform transform hover:scale-125">
                    <span class="sr-only">Twitter</span>
                    <svg
                        aria-hidden="true"
                        class="w-8 h-8 text-[#1da1f1]"
                        fill="currentColor"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                    >
                        <path
                        d="M19.633,7.997c0.013,0.175,0.013,0.349,0.013,0.523c0,5.325-4.053,11.461-11.46,11.461c-2.282,0-4.402-0.661-6.186-1.809 c0.324,0.037,0.636,0.05,0.973,0.05c1.883,0,3.616-0.636,5.001-1.721c-1.771-0.037-3.255-1.197-3.767-2.793 c0.249,0.037,0.499,0.062,0.761,0.062c0.361,0,0.724-0.05,1.061-0.137c-1.847-0.374-3.23-1.995-3.23-3.953v-0.05 c0.537,0.299,1.16,0.486,1.82,0.511C3.534,9.419,2.823,8.184,2.823,6.787c0-0.748,0.199-1.434,0.548-2.032 c1.983,2.443,4.964,4.04,8.306,4.215c-0.062-0.3-0.1-0.611-0.1-0.923c0-2.22,1.796-4.028,4.028-4.028 c1.16,0,2.207,0.486,2.943,1.272c0.91-0.175,1.782-0.512,2.556-0.973c-0.299,0.935-0.936,1.721-1.771,2.22 c0.811-0.088,1.597-0.312,2.319-0.624C21.104,6.712,20.419,7.423,19.633,7.997z"
                        ></path>
                    </svg>
                </a>
                @endif
                @if($profileData['instagram'])
                <a href="{{ $profileData['instagram'] }}" target="_blank" class="transition-transform transform hover:scale-125">
                    <span class="sr-only">Instagram</span>
                    
                    <svg
                        aria-hidden="true"
                        class="w-8 h-8 text-[#c13584]"
                        fill="currentColor"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                    >
                        <path d="M11.999,7.377c-2.554,0-4.623,2.07-4.623,4.623c0,2.554,2.069,4.624,4.623,4.624c2.552,0,4.623-2.07,4.623-4.624 C16.622,9.447,14.551,7.377,11.999,7.377L11.999,7.377z M11.999,15.004c-1.659,0-3.004-1.345-3.004-3.003 c0-1.659,1.345-3.003,3.004-3.003s3.002,1.344,3.002,3.003C15.001,13.659,13.658,15.004,11.999,15.004L11.999,15.004z"></path>
                        <circle cx="16.806" cy="7.207" r="1.078"></circle>
                        <path d="M20.533,6.111c-0.469-1.209-1.424-2.165-2.633-2.632c-0.699-0.263-1.438-0.404-2.186-0.42 c-0.963-0.042-1.268-0.054-3.71-0.054s-2.755,0-3.71,0.054C7.548,3.074,6.809,3.215,6.11,3.479C4.9,3.946,3.945,4.902,3.477,6.111 c-0.263,0.7-0.404,1.438-0.419,2.186c-0.043,0.962-0.056,1.267-0.056,3.71c0,2.442,0,2.753,0.056,3.71 c0.015,0.748,0.156,1.486,0.419,2.187c0.469,1.208,1.424,2.164,2.634,2.632c0.696,0.272,1.435,0.426,2.185,0.45 c0.963,0.042,1.268,0.055,3.71,0.055s2.755,0,3.71-0.055c0.747-0.015,1.486-0.157,2.186-0.419c1.209-0.469,2.164-1.424,2.633-2.633 c0.263-0.7,0.404-1.438,0.419-2.186c0.043-0.962,0.056-1.267,0.056-3.71s0-2.753-0.056-3.71C20.941,7.57,20.801,6.819,20.533,6.111z M19.315,15.643c-0.007,0.576-0.111,1.147-0.311,1.688c-0.305,0.787-0.926,1.409-1.712,1.711c-0.535,0.199-1.099,0.303-1.67,0.311 c-0.95,0.044-1.218,0.055-3.654,0.055c-2.438,0-2.687,0-3.655-0.055c-0.569-0.007-1.135-0.112-1.669-0.311 c-0.789-0.301-1.414-0.923-1.719-1.711c-0.196-0.534-0.302-1.099-0.311-1.669c-0.043-0.95-0.053-1.218-0.053-3.654 c0-2.437,0-2.686,0.053-3.655c0.007-0.576,0.111-1.146,0.311-1.687c0.305-0.789,0.93-1.41,1.719-1.712 c0.534-0.198,1.1-0.303,1.669-0.311c0.951-0.043,1.218-0.055,3.655-0.055c2.437,0,2.687,0,3.654,0.055 c0.571,0.007,1.135,0.112,1.67,0.311c0.786,0.303,1.407,0.925,1.712,1.712c0.196,0.534,0.302,1.099,0.311,1.669 c0.043,0.951,0.054,1.218,0.054,3.655c0,2.436,0,2.698-0.043,3.654H19.315z"></path>
                    </svg>
                </a>
                @endif
                @if($profileData['facebook'])
                <a href="{{ $profileData['facebook'] }}" target="_blank" class="transition-transform transform hover:scale-125">
                    <span class="sr-only">Facebook</span>
                    <svg
                        aria-hidden="true"
                        class="w-8 h-8 text-[#4267B2]"
                        fill="currentColor"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                    >
                        <path d="M12.001,2.002c-5.522,0-9.999,4.477-9.999,9.999c0,4.99,3.656,9.126,8.437,9.879v-6.988h-2.54v-2.891h2.54V9.798 c0-2.508,1.493-3.891,3.776-3.891c1.094,0,2.24,0.195,2.24,0.195v2.459h-1.264c-1.24,0-1.628,0.772-1.628,1.563v1.875h2.771 l-0.443,2.891h-2.328v6.988C18.344,21.129,22,16.992,22,12.001C22,6.479,17.523,2.002,12.001,2.002z"></path>
                    </svg>
                </a>
                @endif
                @if($profileData['reddit'])
                <a href="{{ $profileData['reddit'] }}" target="_blank" class="transition-transform transform hover:scale-125">
                    <span class="sr-only">Reddit</span>
                    <svg
                        aria-hidden="true"
                        class="w-8 h-8 text-[#FF4500]"
                        fill="currentColor"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                    >
                        <circle cx="9.67" cy="13" r="1.001"></circle>
                        <path d="M14.09,15.391C13.482,15.826,12.746,16.041,12,16c-0.744,0.034-1.479-0.189-2.081-0.63c-0.099-0.081-0.242-0.081-0.342,0 c-0.115,0.095-0.132,0.265-0.037,0.38c0.71,0.535,1.582,0.809,2.471,0.77c0.887,0.039,1.76-0.233,2.469-0.77v0.04 c0.109-0.106,0.111-0.285,0.006-0.396C14.377,15.284,14.2,15.282,14.09,15.391z M14.299,12.04c-0.552,0-0.999,0.448-0.999,1.001 c0,0.552,0.447,0.999,0.999,0.999l-0.008,0.039c0.016,0.002,0.033,0,0.051,0c0.551-0.021,0.979-0.487,0.958-1.038 C15.3,12.488,14.854,12.04,14.299,12.04z"></path>
                        <path d="M12,2C6.479,2,2,6.477,2,12c0,5.521,4.479,10,10,10c5.521,0,10-4.479,10-10C22,6.477,17.521,2,12,2z M17.859,13.33 c0.012,0.146,0.012,0.293,0,0.439c0,2.24-2.609,4.062-5.83,4.062c-3.221,0-5.83-1.82-5.83-4.062c-0.012-0.146-0.012-0.293,0-0.439 c-0.145-0.066-0.275-0.155-0.392-0.264c-0.587-0.553-0.614-1.478-0.063-2.063c0.552-0.588,1.477-0.616,2.063-0.063 c1.152-0.781,2.509-1.209,3.899-1.23l0.743-3.47c0-0.001,0-0.002,0-0.004C12.487,6.069,12.652,5.964,12.82,6l2.449,0.49 c0.16-0.275,0.44-0.458,0.758-0.492c0.55-0.059,1.041,0.338,1.102,0.888c0.059,0.549-0.338,1.042-0.889,1.101 c-0.549,0.059-1.041-0.338-1.102-0.887L13,6.65l-0.649,3.12c1.375,0.029,2.711,0.457,3.85,1.23c0.259-0.248,0.599-0.393,0.957-0.406 c0.807-0.029,1.482,0.6,1.512,1.406C18.68,12.563,18.363,13.08,17.859,13.33z"></path>
                    </svg>
                </a>
                @endif
                @if($profileData['website'])
                <a href="{{ $profileData['website'] }}" target="_blank" class="transition-transform transform hover:scale-125">
                    <span class="sr-only">Website</span>
                    <svg
                        aria-hidden="true"
                        class="w-8 h-8 text-purple-500"
                        fill="currentColor"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                    >
                        <path d="M12,2C6.486,2,2,6.486,2,12s4.486,10,10,10s10-4.486,10-10S17.514,2,12,2z M19.931,11h-2.764 c-0.116-2.165-0.73-4.3-1.792-6.243C17.813,5.898,19.582,8.228,19.931,11z M12.53,4.027c1.035,1.364,2.427,3.78,2.627,6.973H9.03 c0.139-2.596,0.994-5.028,2.451-6.974C11.653,4.016,11.825,4,12,4C12.179,4,12.354,4.016,12.53,4.027z M8.688,4.727 C7.704,6.618,7.136,8.762,7.03,11H4.069C4.421,8.204,6.217,5.857,8.688,4.727z M4.069,13h2.974c0.136,2.379,0.665,4.478,1.556,6.23 C6.174,18.084,4.416,15.762,4.069,13z M11.45,19.973C10.049,18.275,9.222,15.896,9.041,13h6.113 c-0.208,2.773-1.117,5.196-2.603,6.972C12.369,19.984,12.187,20,12,20C11.814,20,11.633,19.984,11.45,19.973z M15.461,19.201 c0.955-1.794,1.538-3.901,1.691-6.201h2.778C19.587,15.739,17.854,18.047,15.461,19.201z"></path>
                    </svg>
                </a>
                @endif
                @if($profileData['email'])
                <a href="mailto:{{ $profileData['email'] }}" target="_blank" class="transition-transform transform hover:scale-125">
                    <span class="sr-only">Email</span>
                    <svg
                        aria-hidden="true"
                        class="w-8 h-8 text-purple-500"
                        fill="currentColor"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                    >
                        <path d="M20,4H4C2.897,4,2,4.897,2,6v12c0,1.103,0.897,2,2,2h16c1.103,0,2-0.897,2-2V6C22,4.897,21.103,4,20,4z M20,6v0.511 l-8,6.223L4,6.512V6H20z M4,18V9.044l7.386,5.745C11.566,14.93,11.783,15,12,15s0.434-0.07,0.614-0.211L20,9.044L20.002,18H4z"></path>
                    </svg>
                </a>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="mt-12 container mx-auto sm:px-6 lg:px-8">
    <div class="overflow-x-auto tab-scroller">
        <div class="text-center whitespace-nowrap">
            <x-account.tab-link :href="route('account.items', $address)" class :active="request()->routeIs('account.items')">
                {{ __('Items') }}
            </x-account.tab-link>
            <x-account.tab-link :href="route('account.created', $address)" class :active="request()->routeIs('account.created')">
                {{ __('Created') }}
            </x-account.tab-link>
            @if($address == session('publicKey'))
            <x-account.tab-link :href="route('account.offers-made', $address)" :active="request()->routeIs('account.offers-made')">
                {{ __('Offers Made') }}
            </x-account.tab-link>
            <x-account.tab-link :href="route('account.offers-received', $address)" :active="request()->routeIs('account.offers-received')">
                {{ __('Offers Received') }}
            </x-account.tab-link>
            <x-account.tab-link :href="route('account.claimable-balances', $address)" :active="request()->routeIs('account.claimable-balances')">
                {{ __('Claimable Balances') }}
            </x-account.tab-link>
            <x-account.tab-link :href="route('settings.profile')" :active="request()->routeIs('settings.profile')">
                {{ __('Settings') }}
            </x-account.tab-link>
            @endif
        </div>
    </div>
</div>