@props(['active'])

@php
$classes = ($active ?? false)
            ? 'block pl-3 pr-4 py-2 border-l-4 border-purple-500 text-base font-medium text-purple-500 bg-gray-100 dark:bg-coolGray-800 focus:outline-none focus:text-purple-500 focus:bg-gray-100 dark:focus:bg-coolGray-800 focus:border-purple-500 transition duration-150 ease-in-out'
            : 'block pl-3 pr-4 py-2 border-l-4 border-transparent text-base font-medium text-gray-600 dark:text-gray-400 hover:text-gray-800 dark:hover:text-gray-300 hover:border-gray-300 dark:hover:border-gray-400 focus:outline-none focus:text-gray-800 dark:focus:text-gray-300 focus:border-gray-300 dark:focus:border-gray-400 transition duration-150 ease-in-out';
@endphp

<a {{ $attributes->merge(['class' => $classes]) }}>
    {{ $slot }}
</a>
