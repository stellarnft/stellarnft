@props(['align' => 'right', 'width' => '48', 'wrapperClass' => '', 'contentClasses' => 'py-1 bg-white dark:bg-gray-700'])

@php
switch ($align) {
    case 'left':
        $alignmentClasses = 'origin-top-left left-0';
        break;
    case 'top':
        $alignmentClasses = 'origin-top';
        break;
    case 'right':
    default:
        $alignmentClasses = 'origin-top-right right-0';
        break;
}

switch ($width) {
    case '48':
        $width = 'w-48';
        break;
}
@endphp

<dropdown v-slot="{ open, toggle, hide, show }">
    <div class="relative {{ $wrapperClass }}" @mouseover="show" @mouseleave="hide" v-click-outside="hide">
        <div @click="show">
            {{ $trigger }}
        </div>

        <transition
            enter-active-class="transition ease-out duration-200"
            enter-class="transform opacity-0"
            enter-to-class="transform opacity-100"
            leave-active-class="transition ease-in duration-75"
            leave-class="transform opacity-100"
            leave-to-class="transform opacity-0"
        >
        <div v-show="open"
                class="absolute z-50 pt-2 {{ $width }} rounded-md shadow-lg {{ $alignmentClasses }}"
                style="display: none;"
                @click="hide">
            <div class="rounded-md ring-1 ring-black ring-opacity-5 {{ $contentClasses }}">
                {{ $content }}
            </div>
        </div>
        </transition>
    </div>
</dropdown>
