@props(['type' => 'warning', 'text' => 'text-sm'])

@php
switch ($type) {
    case 'success':
        $classes = 'block text-left text-green-600 bg-green-200 border border-green-400 p-4 rounded-sm';
        break;
    case 'danger':
        $classes = 'block text-left text-red-600 bg-red-200 border border-red-400 p-4 rounded-sm';
        break;
    default:
        $classes = 'block text-left text-yellow-600 bg-yellow-200 border border-yellow-400 p-4 rounded-sm';
        break;
}

$classes .= ' ' . $text;
@endphp

<div {{ $attributes->merge(['class' => $classes]) }} role="alert">
    {{ $slot }}
</div>
