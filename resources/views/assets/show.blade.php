@extends('layouts.app')

@section('title', $asset->version->name.' | '.config('app.name'))

@section('meta')
<meta name="robots" content="noindex">
<meta name="description" content="{{ strip_tags($asset->version->description) }}" />
<meta property="og:description" content="{{ strip_tags($asset->version->description) }}" />
<meta property="og:title" content="{{ $asset->version->name.' | '.config('app.name') }}" />
<meta property="og:image" content="{{ $asset->version->thumb(992) }}" />
@endsection

@section('content')
    <div class="py-6 mx-auto">
        <div class="max-w-screen-xl w-full mx-auto sm:px-4">

            @if(session('auth'))

                @if(!$asset->active)
                <assets-activate :test="{{ App::environment('production') ? 'false' : 'true' }}" wallet="{{ session('wallet') }}" user-public-key="{{ session('publicKey') }}" escrow-public-key="{{ $asset->escrow_public_key }}" #default="{ show }">
                    <x-alert class="mb-6">
                        <div class="text-lg">The item is not activated. <span @click="show" class="underline cursor-pointer font-medium">Activate</span></div>
                    </x-alert>
                </assets-activate>
                @endif

            @endif


            @include('assets.show.title', ['asset' => $asset, 'class' => 'block px-4 sm:px-0 md:hidden mb-6'])


            <div class="md:grid md:grid-cols-12 md:gap-6">
                <div class="md:col-span-5">
                    <div class="relative bg-white dark:bg-gray-700 sm:border border-gray-300 dark:border-gray-400 sm:rounded overflow-hidden h-60 md:h-96 lg:h-128" id="asset-image-wrapper">
                        <v-img full-url="{{ $asset->version->thumb(1920) }}" src="{{ $asset->version->thumb(992) }}" img-class="object-contain w-full h-full"></v-img>
                        <assets-detail-media video="{{ $asset->version->video ? $asset->version->video->url : '' }}" audio="{{ $asset->version->audio ? $asset->version->audio->url : '' }}"></assets-detail-media>
                        @if($asset->verified)
                        <div class="absolute top-3 right-3 bg-green-600 p-1 rounded-full">
                            <div class="text-white tooltip tooltip-left" data-tooltip="Verified">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 13l4 4L19 7" />
                                </svg>
                            </div>
                        </div>
                        @endif
                    </div>

                    @if(!empty(session('publicKey')))
                        @if($asset->active && $asset->owner == session('publicKey'))
                        <div class="mt-6 bg-white dark:bg-gray-700 border border-gray-300 dark:border-gray-400 sm:rounded overflow-hidden">
                            <x-button class="w-full justify-center" :href="$asset->edit_url">Edit</x-button>
                        </div>
                        @endif

                        @if($asset->active && $asset->owner == session('publicKey'))
                        <div class="mt-4 bg-white dark:bg-gray-700 border border-gray-300 dark:border-gray-400 sm:rounded overflow-hidden">
                            <assets-delete action="{{ $asset->delete_url }}" :test="{{ App::environment('production') ? 'false' : 'true' }}" wallet="{{ session('wallet') }}" user-public-key="{{ session('publicKey') }}" escrow-public-key="{{ $asset->escrow_public_key }}" #default="{ show }">
                                <x-button @click="show" class="w-full justify-center">Delete</x-button>    
                            </assets-delete>
                        </div>
                        @endif
                    @endif

                    @include('assets.show.dataentries', ['dataentries' => $asset->dataentries, 'asset' => $asset])
                </div>

                <div class="md:col-span-7">
                    @include('assets.show.title', ['asset' => $asset, 'class' => 'hidden md:block'])

                    <div class="mt-6 p-3 bg-white dark:bg-gray-700 border border-gray-300 dark:border-gray-400 sm:rounded">
                        <div class="text-gray-400">Current price</div>
                        <div class="mt-1">
                            <div class="flex items-center">
                                <div class="tooltip tooltip-right sm:tooltip-top mr-2" data-tooltip="XLM">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="flex-none h-6 w-6" fill="currentColor" viewBox="0 0 236.36 200" xml:space="preserve">
                                        <path d="M203,26.16l-28.46,14.5-137.43,70a82.49,82.49,0,0,1-.7-10.69A81.87,81.87,0,0,1,158.2,28.6l16.29-8.3,2.43-1.24A100,100,0,0,0,18.18,100q0,3.82.29,7.61a18.19,18.19,0,0,1-9.88,17.58L0,129.57V150l25.29-12.89,0,0,8.19-4.18,8.07-4.11v0L186.43,55l16.28-8.29,33.65-17.15V9.14Z"/>
                                        <path d="M236.36,50,49.78,145,33.5,153.31,0,170.38v20.41l33.27-16.95,28.46-14.5L199.3,89.24A83.45,83.45,0,0,1,200,100,81.87,81.87,0,0,1,78.09,171.36l-1,.53-17.66,9A100,100,0,0,0,218.18,100c0-2.57-.1-5.14-.29-7.68a18.2,18.2,0,0,1,9.87-17.58l8.6-4.38Z"/>
                                    </svg>
                                </div>
                                <span class="text-2xl font-medium">
                                    {{ formatPrice($asset->version->price) }}
                                    <span class="text-sm font-normal text-gray-400">(${{ \App\Helpers\MarketplaceHelper::getXLMPrice($asset->version->price, true) }})</span>
                                </span>
                            </div>
                        </div>
                    </div>

                    @includeWhen(\App\Helpers\BountyHelper::showBountyInfo() || $asset->owner == session('publicKey'), 'assets.show.bounty-info', ['asset' => $asset])

                    @if(!empty($asset->version->lockable))
                    <v-dropdown-list :default-state="false" name="asset-unlockable-content" class="mt-6 bg-white dark:bg-gray-700 border border-gray-300 dark:border-gray-400 sm:rounded overflow-hidden">
                        <template #header>
                            <div class="flex items-center">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 mr-2 text-purple-500" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 15v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2zm10-10V7a4 4 0 00-8 0v4h8z" />
                                </svg>
                                <span class="font-medium tracking-tight">Includes unlockable content</span>
                            </div>
                        </template>
                        <template #body>
                            @if($asset->active && $asset->owner == session('publicKey'))
                            <div class="p-3">
                                {{ $asset->version->lockable }}
                            </div>
                            @else
                            <div class="p-3">
                                <div class="animate-pulse text-sm space-y-1">
                                    <div class="w-full bg-gray-400 text-gray-400 rounded-lg">...</div>
                                    <div class="w-full bg-gray-400 text-gray-400 rounded-lg">...</div>
                                    <div class="w-full bg-gray-400 text-gray-400 rounded-lg">...</div>
                                    <div class="w-full bg-gray-400 text-gray-400 rounded-lg">...</div>
                                </div>
                                <div class="mt-4 text-gray-300">This content can only be unlocked and revealed by the owner of this item.</div>
                            </div>
                            @endif
                        </template>
                    </v-dropdown-list>
                    @endif

                    <v-dropdown-list name="asset-description" class="mt-6 bg-white dark:bg-gray-700 border border-gray-300 dark:border-gray-400 sm:rounded overflow-hidden">
                        <template #header>
                            <span class="font-medium tracking-tight">Description</span>
                        </template>
                        <template #body>
                            <div class="p-3">{!! nl2br(strip_tags($asset->version->description)) !!}</div>
                        </template>
                    </v-dropdown-list>

                    <v-dropdown-list name="asset-offers" class="mt-6 bg-white dark:bg-gray-700 border border-gray-300 dark:border-gray-400 sm:rounded overflow-hidden">
                        <template #header>
                            <span class="font-medium tracking-tight">Offers</span>
                        </template>
                        <template #body>
                            @if($asset->owner)
                            <div>  
                                @if($buyOffers->isNotEmpty())
                                <div class="overflow-auto max-h-96">
                                    <table class="min-w-full divide-y divide-gray-300">
                                        <thead>
                                            <tr>
                                                <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-400 uppercase tracking-wider">
                                                    From
                                                </th>
                                                <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-400 uppercase tracking-wider">
                                                    Price
                                                </th>
                                                <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-400 uppercase tracking-wider">
                                                    XDR
                                                </th>
                                                <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-400 uppercase tracking-wider">
                                                    Created
                                                </th>
                                                <th scope="col" class="relative px-3 py-3"></th>
                                            </tr>
                                        </thead>
                                        <tbody class="bg-white divide-y divide-gray-300">
                                            @foreach($buyOffers as $offer)
                                            <tr class="bg-gray-50">
                                                <td class="px-3 py-4 whitespace-nowrap">
                                                    <a class="text-purple-500" target="new-offer-from"  href="{{ route('account.items', $offer->from) }}">{{ truncStellarAddress($offer->from) }}</a>
                                                </td>
                                                <td class="px-3 py-4 whitespace-nowrap">
                                                    <div class="flex items-center">
                                                        <div class="tooltip tooltip-top mr-1" data-tooltip="XLM">
                                                            <svg xmlns="http://www.w3.org/2000/svg" class="flex-none h-4 w-4" fill="currentColor" viewBox="0 0 236.36 200" xml:space="preserve">
                                                                <path d="M203,26.16l-28.46,14.5-137.43,70a82.49,82.49,0,0,1-.7-10.69A81.87,81.87,0,0,1,158.2,28.6l16.29-8.3,2.43-1.24A100,100,0,0,0,18.18,100q0,3.82.29,7.61a18.19,18.19,0,0,1-9.88,17.58L0,129.57V150l25.29-12.89,0,0,8.19-4.18,8.07-4.11v0L186.43,55l16.28-8.29,33.65-17.15V9.14Z"/>
                                                                <path d="M236.36,50,49.78,145,33.5,153.31,0,170.38v20.41l33.27-16.95,28.46-14.5L199.3,89.24A83.45,83.45,0,0,1,200,100,81.87,81.87,0,0,1,78.09,171.36l-1,.53-17.66,9A100,100,0,0,0,218.18,100c0-2.57-.1-5.14-.29-7.68a18.2,18.2,0,0,1,9.87-17.58l8.6-4.38Z"/>
                                                            </svg>
                                                        </div>
                                                        <span>
                                                            {{ formatPrice($offer->price) }}
                                                            <span class="text-sm font-normal text-gray-400">(${{ \App\Helpers\MarketplaceHelper::getXLMPrice($offer->price, true) }})</span>
                                                        </span>
                                                    </div>
                                                </td>
                                                <td class="px-3 py-4 whitespace-nowrap">
                                                    <a href="{{ $offer->lab_url }}" target="new-offer-xdr" class="text-purple-500">View</a>
                                                </td>
                                                <td class="px-3 py-4 whitespace-nowrap">
                                                    {{ $offer->created_at->diffForHumans() }}
                                                </td>
                                                <td class="px-3 py-4 whitespace-nowrap text-right text-sm font-medium">
                                                    @if(session('auth'))
                                                        @if($offer->from == session('publicKey'))
                                                        <offer-cancel #default="{ show }" action="{{ route('offers.cancel', $offer->id) }}">
                                                            <x-button @click="show">Cancel</x-button>    
                                                        </offer-cancel>
                                                        @endif
                                                        @if($offer->to == session('publicKey'))
                                                        <offer-accept from-address="{{ $offer->from }}" price="{{ $offer->price / config('site.priceRatio') }}" xdr="{{ $offer->xdr }}" wallet="{{ session('wallet') }}" user-public-key="{{ session('publicKey') }}" action="{{ route('offers.accept', $offer->id) }}" :test="{{ App::environment('production') ? 'false' : 'true' }}" #default="{ show }">
                                                            <x-button @click="show">Accept</x-button>    
                                                        </offer-accept>
                                                        @endif
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                @else
                                <div class="p-3">
                                    <span>No offers yet</span>
                                </div>
                                @endif
                                @if(session('publicKey') != $asset->owner)
                                <div class="border-t border-gray-300 dark:border-gray-400 p-3">
                                    @if(session('auth'))
                                    <offer-make wallet="{{ session('wallet') }}" user-public-key="{{ session('publicKey') }}" xdr-action="{{ $asset->offer_xdr_url }}" action="{{ $asset->make_offer_url }}" :test="{{ App::environment('production') ? 'false' : 'true' }}"></offer-make>
                                    @else
                                    <div class="text-sm"><a class="text-purple-500" href="{{ route('login') }}" target="new">Login</a> to Make Offer</div>
                                    @endif
                                </div>
                                @endif
                            </div>
                            @else
                            <div class="p-3">
                                <span>Something happening, please, come back in a few minutes.</span>
                            </div> 
                            @endif
                        </template>
                    </v-dropdown-list>

                    <v-dropdown-list name="asset-history" class="mt-6 bg-white dark:bg-gray-700 border border-gray-300 dark:border-gray-400 sm:rounded overflow-hidden">
                        <template #header>
                            <span class="font-medium tracking-tight">History</span>
                        </template>
                        <template #body>
                            <div>  
                                @if($activities->isNotEmpty())
                                <div class="overflow-auto max-h-96">
                                    <table class="min-w-full divide-y divide-gray-300">
                                        <thead>
                                            <tr>
                                                <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-400 uppercase tracking-wider">
                                                    Event
                                                </th>
                                                <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-400 uppercase tracking-wider">
                                                    Price
                                                </th>
                                                <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-400 uppercase tracking-wider">
                                                    From
                                                </th>
                                                <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-400 uppercase tracking-wider">
                                                    To
                                                </th>
                                                <th scope="col" class="px-3 py-3 text-left text-xs font-medium text-gray-400 uppercase tracking-wider">
                                                    Date
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody class="bg-white divide-y divide-gray-300">
                                            @foreach($activities as $activity)
                                                @includeIf($activity->type::getInfoTemplate(), ['activity' => $activity])
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                @else
                                <div class="p-3">
                                    <span>No history info</span>
                                </div>
                                @endif
                            </div>
                        </template>
                    </v-dropdown-list>
                </div>
            </div>
        </div>
    </div>
@endsection