@extends('layouts.app')

@section('title', __('Edit The Item').' | '.config('app.name'))

@section('content')
    <div class="py-6 sm:max-w-5xl mx-auto">
        <div class="hero-headline px-4 sm:text-center">
            <div class="sm:text-center">
                <h1 class="text-4xl tracking-tight font-thin text-purple-500 sm:text-5xl md:text-6xl">
                    <div class="block">Edit The Item</div>
                </h1>
            </div>
        </div>

        <assets-update :categories='@json(\App\Helpers\MarketplaceHelper::getCategories())' :asset-data='@json($assetData)' wallet="{{ session('wallet') }}" user-public-key="{{ session('publicKey') }}" action="{{ $asset->update_url }}" :test="{{ App::environment('production') ? 'false' : 'true' }}"></assets-update>
    </div>
@endsection