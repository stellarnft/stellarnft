<v-dropdown-list name="asset-description" class="mt-6 bg-white dark:bg-gray-700 border border-gray-300 dark:border-gray-400 sm:rounded overflow-hidden">
    <template #header>
        <span class="font-medium tracking-tight">Chain Data</span>
    </template>
    <template #body>
        <div class="p-3 space-y-2">

            <div>
                <div class="block font-medium text-sm">NFT Address</div>
                <div class="mt-1">
                    <a class="text-purple-500" target="new-nft-address" href="https://stellar.expert/explorer/public/account/{{ $asset->issuer }}">{{ truncStellarAddress($asset->issuer) }}</a>
                </div>
            </div>

            @if($asset->author)
            <div>
                <div class="block font-medium text-sm">NFT Author</div>
                <div class="mt-1">
                    <a class="text-purple-500" target="new-nft-owner" href="{{ route('account.created', $asset->author) }}">{{ truncStellarAddress($asset->author) }}</a>
                </div>
            </div>
            @endif

            @if(!empty($dataentries['name']))
            <div>
                <div class="block font-medium text-sm">Name</div>
                <div class="mt-1">
                    <span class="break-words">{{ $dataentries['name'] }}</span>
                </div>
            </div>
            @endif

            @if(!empty($dataentries['ipfs']))
            <div>
                <div class="block font-medium text-sm">IPFS</div>
                <div class="mt-1">
                    <a href="https://ipfs.io/ipfs/{{ $dataentries['ipfs'] }}" target="new-ipfs-file" class="text-purple-500">{{ truncStellarAddress($dataentries['ipfs']) }}</a>
                </div>
            </div>
            @endif

            @if(!empty($dataentries['royaltyAmount']) && !empty($dataentries['royaltyReceiver']))
            <div>
                <div class="block font-medium text-sm">Royalty</div>
                <div class="mt-1">
                    {{ intval($dataentries['royaltyAmount']) * 0.0000001 }}% to <a class="text-purple-500" target="new-nft-royalty" href="https://stellar.expert/explorer/public/account/{{ $dataentries['royaltyReceiver'] }}">{{ truncStellarAddress($dataentries['royaltyReceiver']) }}</a>
                </div>
            </div>
            @endif

            @php
                unset($dataentries['name']);
                unset($dataentries['ipfs']);
                unset($dataentries['royaltyAmount']);
                unset($dataentries['royaltyReceiver']);
            @endphp

            @foreach($dataentries as $name => $value)
                <div>
                    <div class="block font-medium text-sm">{{ $name }}</div>
                    <div class="mt-1">
                        <span class="break-words">{{ $value }}</span>
                    </div>
                </div>
            @endforeach
        </div>
    </template>
</v-dropdown-list>