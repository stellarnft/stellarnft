<div class="relative {{ $class }}">
    <div class="pr-20">
        <div class="break-words text-2xl sm:break-all sm:text-4xl">{{ $asset->version->name }}</div>
        @if($asset->owner)
        <div>
            <span>Owned by </span>
            <a class="text-purple-500" href="{{ route('account.items', $asset->owner) }}">{{ truncStellarAddress($asset->owner) }}</a>
        </div>
        @endif
    </div>
    <div class="absolute top-0 right-4 sm:right-0 z-10">
        <div class="bg-white rounded flex items-center shadow divide-x">
            <div class="tooltip tooltip-left" data-tooltip="Share">
                <assets-share title="{{ $asset->version->name }}" url="{{ $asset->url }}" description="{{ $asset->version->description }}" #default="{ show }">
                <button @click="show" class="p-2 text-purple-500 rounded-l hover:bg-gray-200 transition duration-150 ease-in-out focus:outline-none">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8.684 13.342C8.886 12.938 9 12.482 9 12c0-.482-.114-.938-.316-1.342m0 2.684a3 3 0 110-2.684m0 2.684l6.632 3.316m-6.632-6l6.632-3.316m0 0a3 3 0 105.367-2.684 3 3 0 00-5.367 2.684zm0 9.316a3 3 0 105.368 2.684 3 3 0 00-5.368-2.684z" />
                    </svg>
                </button>
                </assets-share>
            </div>
            <div class="tooltip tooltip-left" data-tooltip="Report">
                <assets-report action="{{ $asset->report_url }}" #default="{ show }">
                <button @click="show" class="p-2 text-purple-500 rounded-r hover:bg-gray-200 transition duration-150 ease-in-out focus:outline-none">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 21v-4m0 0V5a2 2 0 012-2h6.5l1 1H21l-3 6 3 6h-8.5l-1-1H5a2 2 0 00-2 2zm9-13.5V9" />
                    </svg>
                </button>
                </assets-report>
            </div>
        </div>
    </div>
</div>