@extends('dashboard.layouts.app')

@section('content')

    <div class="container mx-auto sm:px-6 lg:px-8 py-6">
        <div class="hero-headline px-4 sm:text-center">
            <div class="sm:text-center">
                <h1 class="text-4xl tracking-tight font-thin text-purple-500 sm:text-5xl md:text-6xl">
                    <div class="block">Categories</div>
                </h1>
                <p class="mt-3 text-sm text-gray-400 sm:max-w-xl sm:mx-auto">Category Add</p>
            </div>
        </div>

        <div class="mt-14 space-y-10">

            <div class="relative w-full mt-6 p-6 bg-white dark:bg-gray-700 shadow overflow-hidden sm:rounded-lg">
                <form action="{{ route('dashboard.categories.store') }}" method="POST">
                    @csrf
                    <div class="space-y-6">
                        <div>
                            <label for="name" class="block text-sm font-medium text-gray-700">Name</label>
                            <input type="text" name="name" id="name" value="{{ old('name') }}" class="mt-1 focus:ring-purple-500 focus:border-purple-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                            @error('name')
                            <p class="mt-2 text-sm text-red-600 validation-error">{{ $message }}</p>
                            @endif
                        </div>
                        <div>
                            <label for="parent_id" class="block text-sm font-medium text-gray-700">Parent ID</label>
                            <input type="text" name="parent_id" id="parent_id" value="{{ old('parent_id') }}" class="mt-1 focus:ring-purple-500 focus:border-purple-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                            @error('parent_id')
                            <p class="mt-2 text-sm text-red-600 validation-error">{{ $message }}</p>
                            @endif
                        </div>
                        <div>
                            <label for="icon" class="block text-sm font-medium text-gray-700">Icon</label>
                            <input type="text" name="icon" id="icon" value="{{ old('icon') }}" class="mt-1 focus:ring-purple-500 focus:border-purple-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                            @error('icon')
                            <p class="mt-2 text-sm text-red-600 validation-error">{{ $message }}</p>
                            @endif
                        </div>
                        <div>
                            <label for="sort" class="block text-sm font-medium text-gray-700">Sort</label>
                            <input type="text" name="sort" id="sort" value="{{ old('sort') }}" class="mt-1 focus:ring-purple-500 focus:border-purple-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                            @error('sort')
                            <p class="mt-2 text-sm text-red-600 validation-error">{{ $message }}</p>
                            @endif
                        </div>
                        <div>
                            <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-purple-500 hover:bg-purple-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-purple-500">
                                Add Category
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            
        </div>
    </div>
@endsection
