<div :class="{'block': mobileMenuOpen, 'hidden': !mobileMenuOpen}" class="lg:block fixed flex flex-col top-0 left-0 w-64 bg-white shadow border-l border-gray-100 h-full shadow-lg">
    <div class="overflow-y-auto overflow-x-hidden flex-grow">
        <ul class="flex flex-col py-6 space-y-1">
            @foreach($menu as $item)
                @if($item['type'] == 'heading')
                    <li class="px-5">
                        <div class="flex flex-row items-center h-8">
                            <div class="flex font-semibold text-sm text-gray-400 my-4 font-sans uppercase">{{ $item['name'] }}</div>
                        </div>
                    </li>
                @elseif($item['type'] == 'item')
                    @php
                    $itemClass = "relative flex flex-row items-center h-11 focus:outline-none  border-l-4 border-transparen pr-6";
                    if(request()->routeIs($item['route'])) {
                        $itemClass .= 'bg-gray-200 text-purple-500 border-purple-500';
                    } else {
                        $itemClass .= 'hover:bg-gray-200 hover:text-purple-500 hover:border-purple-500';
                    }
                    @endphp
                    <li>
                        <a @if(!empty($item['target'])) target="{{ $item['target'] }}" @endif href="{{ $item['link'] }}" class="{{ $itemClass }}">
                            @if($item['icon'])
                            <span class="inline-flex justify-center items-center ml-4">
                                <svg class="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                {!! $item['icon'] !!}
                                </svg>
                            </span>
                            @endif
                            <span class="ml-2 font-semibold text-sm tracking-wide truncate font-sans">{{ $item['name'] }}</span>
                        </a>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>
</div>