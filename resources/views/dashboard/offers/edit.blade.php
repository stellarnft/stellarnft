@extends('dashboard.layouts.app')

@section('content')

    <div class="container mx-auto sm:px-6 lg:px-8 py-6">
        <div class="hero-headline px-4 sm:text-center">
            <div class="sm:text-center">
                <h1 class="text-4xl tracking-tight font-thin text-purple-500 sm:text-5xl md:text-6xl">
                    <div class="block">Assets</div>
                </h1>
                <p class="mt-3 text-sm text-gray-400 sm:max-w-xl sm:mx-auto">Asset Edit</p>
            </div>
        </div>

        <div class="mt-14 space-y-10">

            <div class="relative w-full mt-6 p-6 bg-white dark:bg-gray-700 shadow overflow-hidden sm:rounded-lg">
                <div class="space-y-4 mb-12">
                    <div>
                        <div class="font-medium">Tradable:</div> 
                        <div class="text-sm">{{ $asset->version->tradable ? 'Yes' : 'No' }}</div>
                    </div>
                    <div>
                        <div class="font-medium">Price:</div> 
                        <div class="text-sm">{{ formatPrice($asset->version->price) }}</div>
                    </div>
                    <div>
                        <div class="font-medium">Min Offer:</div> 
                        <div class="text-sm">{{ formatPrice($asset->version->min_offer) }}</div>
                    </div>
                    <div>
                        <div class="font-medium">Name:</div> 
                        <div class="text-sm">{{ $asset->version->name }}</div>
                    </div>
                    <div>
                        <div class="font-medium">Lockable:</div> 
                        <div class="text-sm">{{ $asset->version->lockable }}</div>
                    </div>
                </div>

                <form action="{{ route('dashboard.assets.update', $asset->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="space-y-6">
                        <div>
                            <div class="flex items-start">
                                <div class="flex items-center h-5">
                                    <input id="banned" name="banned" type="checkbox" value="1" @if($asset->banned == 1) checked @endif class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                                </div>
                                <div class="ml-3 text-sm">
                                    <label for="banned" class="font-medium text-gray-700">Banned</label>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="flex items-start">
                                <div class="flex items-center h-5">
                                    <input id="nsfw" name="nsfw" type="checkbox" value="1" @if($asset->version->nsfw == 1) checked @endif class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                                </div>
                                <div class="ml-3 text-sm">
                                    <label for="nsfw" class="font-medium text-gray-700">NSFW</label>
                                </div>
                            </div>
                        </div>
                        <div>
                            <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-purple-500 hover:bg-purple-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-purple-500">
                                Update Asset
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            
        </div>
    </div>
@endsection
