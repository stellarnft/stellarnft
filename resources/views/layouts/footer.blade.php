<footer class="footer bg-white dark:bg-gray-700 mt-auto py-6 px-4 sm:px-0">
    <div class="container mx-auto sm:px-6 lg:px-8">
        <div class="sm:flex flex-wrap items-center justify-between text-gray-400">
            <div class="space-y-2 sm:space-y-0 sm:flex items-center sm:divide-x devide-gray-300 sm:-mx-3">
                <div class="sm:px-3"><a href="{{ route('pages.help') }}" class="hover:text-purple-500">Help</a></div>
                <div class="sm:px-3"><a href="{{ route('pages.faq') }}" class="hover:text-purple-500">FAQ</a></div>
                <div class="sm:px-3"><a href="{{ route('pages.bounty-program') }}" class="hover:text-purple-500">Bounty Program</a></div>
                <div class="sm:px-3"><a href="{{ route('pages.contact') }}" class="hover:text-purple-500">Contact Form</a></div>
                <div class="sm:px-3">
                    <a href="https://bitbucket.org/stellarnft/" target="new-source" class="text-[#2684ff] hover:text-purple-500">
                        <svg class="w-5 h-6" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M22.2 32A16 16 0 0 0 6 47.8a26.35 26.35 0 0 0 .2 2.8l67.9 412.1a21.77 21.77 0 0 0 21.3 18.2h325.7a16 16 0 0 0 16-13.4L505 50.7a16 16 0 0 0-13.2-18.3 24.58 24.58 0 0 0-2.8-.2L22.2 32zm285.9 297.8h-104l-28.1-147h157.3l-25.2 147z"></path></svg>
                    </a>
                </div>
            </div>
            <div class="text-left sm:text-right">
                <div class="mt-2 sm:mt-0 copyright text-gray-400">© {{ date('Y') }} Stellar<span class="text-purple-500">NFT</span></div>
            </div>
        </div>
    </div>
</footer>