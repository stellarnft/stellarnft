@extends('layouts.app')

@section('title', 'Notification Settings | '.config('app.name'))


@section('content')
    <div class="container mx-auto sm:px-6 lg:px-8 py-6">
        <div class="hero-headline px-4 sm:text-center">
            <h1 class="text-4xl tracking-tight font-thin sm:text-5xl md:text-6xl text-purple-500">Settings</h1>
            <p class="mt-3 text-sm text-gray-400 sm:max-w-xl sm:mx-auto">Notification Settings</p>
        </div>

        <div class="mt-8 sm:mt-12">

            <div class="md:grid md:grid-cols-12 md:gap-6">
                <div class="col-span-3">
                    @include('settings.navigation')
                </div>

                <div class="col-span-9">
                    <div class="relative w-full mt-6 px-6 py-4 bg-white dark:bg-gray-700 shadow overflow-hidden sm:rounded-lg">
                        <form method="post" action="{{ route('settings.notifications.set') }}">
                            @csrf
                            <div class="space-y-4">
                                <div>
                                    <label for="email" class="block font-medium text-sm">Email</label>
                                    <div class="mt-1 flex rounded-md shadow-sm">
                                        <input type="text" value="{{ old('email', optional($notifications)['email']) }}" name="email" id="email" class="dark:bg-gray-800 focus:border-purple-500 focus:ring focus:ring-purple-500 focus:ring-opacity-50 flex-1 block w-full rounded-none z-10 rounded-md  border-gray-300 dark:border-gray-400 @error('email') border-red-500 @enderror" placeholder="myemail@gmail.com">
                                    </div>
                                    @error('email')
                                    <p class="mt-2 text-sm text-red-600 validation-error">{{ $message }}</p>
                                    @enderror
                                </div>
                                <div>
                                    <div class="mt-4">
                                        <div class="flex items-start">
                                            <div class="flex items-center h-5">
                                                <input id="offer_received" name="offer_received" {{ old('offer_received', optional($notifications)['offer_received']) == '1' ? 'checked' : '' }} value="1" type="checkbox" class="focus:ring-purple-500 h-4 w-4 text-purple-600 border-gray-300 rounded">
                                            </div>
                                            <label for="offer_received" class="ml-3 block font-medium text-sm">New Offer Received</label>
                                        </div>
                                    </div>
                                    <p class="mt-2 text-sm text-gray-500">There will be a slight delay to prevent spam from malicious users. And you won't receive notifications if you are online.</p>
                                </div>
                                <div>
                                    <div class="mt-4">
                                        <div class="flex items-start">
                                            <div class="flex items-center h-5">
                                                <input id="new_royalty" name="new_royalty" {{ old('new_royalty', optional($notifications)['new_royalty']) == '1' ? 'checked' : '' }} value="1" type="checkbox" class="focus:ring-purple-500 h-4 w-4 text-purple-600 border-gray-300 rounded">
                                            </div>
                                            <label for="new_royalty" class="ml-3 block font-medium text-sm">New Royalty To Claim</label>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="mt-4">
                                        <div class="flex items-start">
                                            <div class="flex items-center h-5">
                                                <input id="offer_accepted" name="offer_accepted" {{ old('offer_accepted', optional($notifications)['offer_accepted']) == '1' ? 'checked' : '' }} value="1" type="checkbox" class="focus:ring-purple-500 h-4 w-4 text-purple-600 border-gray-300 rounded">
                                            </div>
                                            <label for="offer_accepted" class="ml-3 block font-medium text-sm">My Offer Accepted</label>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <button class="inline-flex items-center font-medium leading-6 bg-gray-100 dark:bg-gray-800 py-2 px-3 rounded text-purple-500 hover:bg-gray-200 dark:hover:bg-gray-600 transition duration-150 ease-in-out focus:outline-none disabled:opacity-25 uppercase w-full justify-center h-12">
                                        <div class="flex items-center">
                                            <div>Update</div>
                                        </div>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection