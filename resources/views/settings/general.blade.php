@extends('layouts.app')

@section('title', 'General Settings | '.config('app.name'))


@section('content')
    <div class="container mx-auto sm:px-6 lg:px-8 py-6">
        <div class="hero-headline px-4 sm:text-center">
            <h1 class="text-4xl tracking-tight font-thin sm:text-5xl md:text-6xl text-purple-500">Settings</h1>
            <p class="mt-3 text-sm text-gray-400 sm:max-w-xl sm:mx-auto">General Settings</p>
        </div>

        <div class="mt-8 sm:mt-12">

            <div class="md:grid md:grid-cols-12 md:gap-6">
                <div class="col-span-3">
                    @include('settings.navigation')
                </div>

                <div class="col-span-9">
                    <div class="relative w-full mt-6 px-6 py-4 bg-white dark:bg-gray-700 shadow overflow-hidden sm:rounded-lg">
                        <form method="post" action="{{ route('settings.general.set') }}">
                            @csrf
                            <div class="space-y-4">
                                <div>
                                    <div class="mt-4">
                                        <div class="flex items-start">
                                            <div class="flex items-center h-5">
                                                <input id="bounty_program" name="bounty_program" {{ old('bounty_program', optional($general)['bounty_program']) == '1' ? 'checked' : '' }} value="1" type="checkbox" class="focus:ring-purple-500 h-4 w-4 text-purple-600 border-gray-300 rounded">
                                            </div>
                                            <label for="bounty_program" class="ml-3 block font-medium text-sm">Show Bounty Program Information</label>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <button class="inline-flex items-center font-medium leading-6 bg-gray-100 dark:bg-gray-800 py-2 px-3 rounded text-purple-500 hover:bg-gray-200 dark:hover:bg-gray-600 transition duration-150 ease-in-out focus:outline-none disabled:opacity-25 uppercase w-full justify-center h-12">
                                        <div class="flex items-center">
                                            <div>Update</div>
                                        </div>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection