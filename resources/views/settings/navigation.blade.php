<div class="relative w-full sm:max-w-5xl mt-6 bg-white dark:bg-gray-700 shadow overflow-hidden sm:rounded-lg">
    <div class="divide-y divide-gray-300">
        <div>
            <a title="General Settings" href="{{ route('settings.general') }}" class="w-full flex font-medium items-center focus:outline-none appearance-none p-4 border-l-4 {{ request()->routeIs('settings.general*') ? 'text-purple-500 border-purple-500' : 'hover:text-purple-500 border-transparent hover:border-purple-500' }}">General Settings</a>
        </div>
        <div>
            <a title="Profile Settings" href="{{ route('settings.profile') }}" class="w-full flex font-medium items-center focus:outline-none appearance-none p-4 border-l-4 {{ request()->routeIs('settings.profile*') ? 'text-purple-500 border-purple-500' : 'hover:text-purple-500 border-transparent hover:border-purple-500' }}">Profile Settings</a>
        </div>
        <div>
            <a title="Notification Settings" href="{{ route('settings.notifications') }}" class="w-full flex font-medium items-center focus:outline-none appearance-none p-4 border-l-4 {{ request()->routeIs('settings.notifications*') ? 'text-purple-500 border-purple-500' : 'hover:text-purple-500 border-transparent hover:border-purple-500' }}">Notification Settings</a>
        </div>
    </div>
</div>